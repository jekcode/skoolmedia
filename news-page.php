<?php /* Template Name: News Page Template */ get_header(); ?>

<?php 

$getimage = tr_posts_field('banner_image');
$image = wp_get_attachment_image_url($getimage);

$background_image  = !empty( $image ) ?  $image : 'http://sm.test/wp-content/uploads/2018/11/patrick-tomasso-71909-unsplash.jpg';
$heading = !empty( tr_posts_field('heading') ) ? tr_posts_field('heading') : 'News and Updates';
$subheading = !empty( tr_posts_field('subheading') ) ? tr_posts_field('subheading') : '';
$color_tint = !empty( tr_posts_field('color_tint') ) ? tr_posts_field('color_tint') : 'blue';

?>
    <!-- Hero Text Intro -->
    <section class="hero-basic" style="background-image: url('<?php echo $background_image ; ?>">
        <div class="w-100 <?php echo $color_tint; ?>-tint">
            <div class="col-lg-6 mx-auto">
                <div class="hero-info">

                    <!-- Careers Page Title -->
                    <div class="title">
                        <h1>
                            <?php echo $heading; ?>
                        </h1>
                    </div>
                    <!-- / Careers Page Title -->

                    <!-- Page Excerpt -->
                    <div class="copy">
                        <p>
                            <?php echo $subheading; ?>
                        </p>
                    </div>
                    <!-- / Careers Page Excerpt -->

                </div>
            </div>
        </div>
    </section>
    <!-- / Hero Text Intro -->

    <!-- Top News -->
    <section class="section-padding-news grey-bg">
        <div class="container">

           <div class="section-title">
                News
            </div>

            <div class="section-sub-title">
            </div>

            <div class="row news-row">

                <?php 

                $args = array(
                    'post_type'      => 'post',
                    //'cat'            => '22,47,67',
                    'orderby'        => 'date',
                    'order'          => 'DESC',
                    'hide_empty'     => 1,
                    //'depth'          => 1,
                    'posts_per_page' => 3
                );

                // the query
                $the_query = new WP_Query( $args ); ?>

                <?php if ( $the_query->have_posts() ) : ?>

                    <!-- pagination here -->

                    <!-- the loop -->
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                       <div class="col-md-4">
                            <!-- News Box -->
                            <div class="news-box">

                                <!-- News Featured Image -->
                                <div class="news-feature-image" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/jessica-ruscello-196422-unsplash.jpg');">

                                    <!-- News Date -->
                                    <div class="news-date">
                                        <p class="day"><?php the_time('d'); ?></p>
                                        <p class="month"><?php the_time('M'); ?></p>
                                    </div>
                                    <!-- / News Date -->

                                    <!-- News Title -->
                                    <!--Important! - Remove 'black-tint' if there is no featured image -->
                                    <div class="display-tr black-tint">
                                        <div class="news-title">
                                            <?php the_title(); ?>
                                        </div>
                                    </div>
                                    <!-- / News Title -->

                                </div>
                                <!-- / News Featured Image -->

                                <!-- News Excerpt -->
                                <div class="news-excerpt">
                                    <?php //the_excerpt(20); ?>
                                    <?php html5wp_excerpt(); ?>
                                </div>
                                <!-- / News Excerpt -->

                                <!-- News link -->
                                <div class="news-link">
                                    <a href="<?php the_permalink(); ?>">
                                            Read more
                                        </a>
                                </div>
                                <!-- / News link -->

                            </div>
                            <!-- / News Box -->
                        </div>


                     <?php endwhile; ?>
                        <!-- end of the loop -->

                        <!-- pagination here -->

                        <?php wp_reset_postdata(); ?>

                    <?php else : ?>
                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>

            <?php get_template_part('pagination'); ?>

            </div>
        </div>
    </section>
    <!-- / Top News -->

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
