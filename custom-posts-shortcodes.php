<?php

add_shortcode( 'show-vacancies', 'career_listing_shortcode' );
function career_listing_shortcode( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'tr_vacancies',
        'posts_per_page' => -1,
        //'order' => 'DESC',
        //'orderby' => 'date',
    ) );
    if ( $query->have_posts() ) { ?>

 <div class="col-md-10 mx-auto">
                <div class="row vacancy-holder">
            <?php $count = 0; ?>
    	<?php while ( $query->have_posts() ) : $query->the_post(); $count++; ?>

    	<!-- Single Vacancy -->
                    <div class="col-12 col-lg-6 vacancy">
                        <div class="vacancy-box shadow-sm">

                            <div class="vacancy-title">
                                <p>
                                    <?php the_title(); ?>
                                </p>
                            </div>

                            <div class="vacancy-expiration">
                                <p>
                                    Closing date: <span><?php echo tr_posts_field('closing_date'); ?></span>
                                </p>
                            </div>

                            <div id="<?php echo $count; ?>" class="collapse vacancy-desc">
                                <div class="vacancy-desc-title">
                                    Job Description
                                </div>

                                <div class="vacancy-desc-info">
                                    <?php echo tr_posts_field('post_content'); ?>
                                </div>
                            </div>
                            
                            <div class="mb-2">
                        <a class="btn btn-red-primary" href="<?php the_permalink(); ?>">
                            Apply
                        </a>
                    </div>

                            <div class="vacancy-icon">
                                <a class="view-more-hr" onclick="" data-toggle="collapse" href="#<?php echo $count; ?>" role="button" aria-expanded="false" aria-controls="">
                                    <i id="view-more-vacancy" class="fa fa-plus-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /Single Vacancy -->

            <?php endwhile;
            wp_reset_postdata(); ?>

            </div>
            </div>
       
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}