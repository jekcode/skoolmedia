<?php // archive-tr_team.php
get_header(); ?>
<section id="content">
    <?php while (have_posts()) : the_post() ?>
        <h1>
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h1>
        <section>
            <?php echo tr_posts_field( "job_title" ); ?>
        </section>
    <?php endwhile; ?>
</section>
<?php get_footer(); ?>