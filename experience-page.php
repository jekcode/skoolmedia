<?php /* Template Name: Experience Page Template */ get_header(); ?>

<?php 

$getimage = tr_posts_field('banner_image');
$image = wp_get_attachment_image_url($getimage);

$background_image  = !empty( $image ) ?  $image : 'http://sm.test/wp-content/uploads/2018/11/patrick-tomasso-71909-unsplash.jpg';
$heading = !empty( tr_posts_field('heading') ) ? tr_posts_field('heading') : 'This is Skool Media';
$subheading = !empty( tr_posts_field('subheading') ) ? tr_posts_field('subheading') : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at auctor turpis, ac feugiat sapien. Maecenas auctor urna egestas, placerat felis a, ultrices dui. Nam quis convallis ex, eu pellentesque diam. Praesent non lacinia risus. Donec pharetra, ipsum non eleifend posuere, lectus nisi suscipit urna, in convallis lectus erat vitae metus. Integer pharetra et enim sed dapibus. Mauris semper purus ipsum, a accumsan tortor molestie sit amet.';
$color_tint = !empty( tr_posts_field('color_tint') ) ? tr_posts_field('color_tint') : 'green';

?>

<!-- Hero Text Intro -->
<section class="hero-basic" style="background-image: url('<?php echo $background_image ; ?>">
    <div class="w-100 <?php echo $color_tint; ?>-tint">
        <div class="col-lg-6 mx-auto">
            <div class="hero-info">

                <!-- Careers Page Title -->
                <div class="title">
                    <h1>
                        <?php echo $heading; ?>
                    </h1>
                </div>
                <!-- / Careers Page Title -->

                <!-- Page Excerpt -->
                <div class="copy">
                    <p>
                        <?php echo $subheading; ?>
                    </p>
                </div>
                <!-- / Careers Page Excerpt -->

            </div>
        </div>
    </div>
</section>
<!-- / Hero Text Intro -->

<section class="section-padding">
        <div class="container">
            <!-- Section title -->
            <div class="section-title">
                Come In!
            </div>
            <!-- / Section title -->

            <!-- Section sub title -->
            <div class="section-sub-title">
            </div>
            <!-- / Section sub title -->

            <div class="col-md-8 mx-auto">
                <div class="row news-row">

                    <!-- Experience Type -->
                    <div class="col-12 col-md-6">
                        <div id="1" class="control-experience orange-experience">
                            <div class="orange-experience">
                                <div class="experience" style="background-image: url('http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/jessica-ruscello-196422-unsplash.jpg');">
                                    <!-- Experience Title -->
                                    <div class="experience-title orange-bg">
                                        <p>Student tech experience centre</p>
                                    </div>
                                    <!-- /Experience Title -->

                                    <!-- Experience Reveal -->
                                    <div class="experience-reveal">
                                        <!-- Trigger Button -->
                                        <a id="reveal-1" href="#experience-1" data-toggle="collapse" data-target="#experience-1" role="button" aria-expanded="false" class="reveal">
                                            Reveal
                                        </a>
                                        <!-- /Trigger Button -->
                                    </div>
                                    <!-- /Experience Reveal -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Experience Type -->
                    
                    <!-- Experience Type -->
                    <div class="col-12 col-md-6">
                        <div id="2" class="control-experience green-experience">
                            <div class="green-experience">
                                <div class="experience" style="background-image: url('http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/jessica-ruscello-196422-unsplash.jpg');">
                                    <!-- Experience Title -->
                                    <div class="experience-title green-bg">
                                        <p>Teachers tech experience centre</p>
                                    </div>
                                    <!-- /Experience Title -->

                                    <!-- Experience Reveal -->
                                    <div class="experience-reveal">
                                        <!-- Trigger Button -->
                                        <a id="reveal-2" href="#experience-2" data-toggle="collapse" data-target="#experience-2" role="button" aria-expanded="false" class="reveal">
                                            Reveal
                                        </a>
                                        <!-- /Trigger Button -->
                                    </div>
                                    <!-- /Experience Reveal -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Experience Type -->

                </div>
            </div>
            
            <!-- Experience Tree -->
            <div id="experience-1" class="collapse">
               
                <div class="col-md-10 mx-auto control-experience-content e-content-box orange-experience">
                </div>


                <!-- Static Placeholder Boxes -->
                <div class="col-md-10 mx-auto">
                    <div class="row">
                        <div class="col-md-4 col-12 placeholder-box orange-experience">
                        </div>

                        <div class="col-md-4 col-12 placeholder-box orange-experience">
                        </div>

                        <div class="col-md-4 col-12 placeholder-box orange-experience">
                        </div>
                    </div>
                </div>
                <!-- / Static Placeholder Boxes -->

                <div class="col-md-11 mx-auto e-content-container">
                    <div id="experienceCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">

                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="carousel-item active">
                                <div class="row">
                                    <!-- experience -->
                                    <div class="col-md-4 col-6">
                                        <div class="e-content">
                                            <a href="#">
                                                <img src="http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/ec-3.jpg" alt="Image" style="max-width:100%; width: 250px;">
                                            </a>
                                        </div>
                                    </div>
                                    <!-- / experience -->

                                    <!-- experience -->
                                    <div class="col-md-4 col-6">
                                        <div class="e-content">
                                            <a href="#">
                                                <img src="http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/ec-2.jpg" alt="Image" style="max-width:100%; width: 250px;">
                                            </a>
                                        </div>
                                    </div>
                                    <!-- / experience -->

                                    <!-- experience -->
                                    <div class="col-md-4 col-6">
                                        <div class="e-content">
                                            <a href="#">
                                                <img src="http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/ec-1.jpg" alt="Image" style="max-width:100%; width: 250px;">
                                            </a>
                                        </div>
                                    </div>
                                    <!-- / experience -->

                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->
                        </div>
                        <!--.carousel-inner-->

                    </div>
                </div>

            </div>
            <!-- / Experience Tree -->
            
            <!-- Experience Tree -->
            <div id="experience-2" class="collapse">
                
                <div class="col-md-10 mx-auto control-experience-content e-content-box green-experience">
                </div>


                <!-- Static Placeholder Boxes -->
                <div class="col-md-10 mx-auto">
                    <div class="row">
                        <div class="col-md-4 col-12 placeholder-box green-experience">
                        </div>

                        <div class="col-md-4 col-12 placeholder-box green-experience">
                        </div>

                        <div class="col-md-4 col-12 placeholder-box green-experience">
                        </div>
                    </div>
                </div>
                <!-- / Static Placeholder Boxes -->

                <div class="col-md-11 mx-auto e-content-container">
                    <div id="experienceCarousel-2" class="carousel slide" data-ride="carousel" data-interval="10000">

                        <!-- Carousel items -->
                        <div class="carousel-item active">
                                <div class="row">
                                    <!-- experience -->
                                    <div class="col-md-4 col-6">
                                        <div class="e-content">
                                            <a href="#">
                                                <img src="http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/ec-3.jpg" alt="Image" style="max-width:100%; width: 250px;">
                                            </a>
                                        </div>
                                    </div>
                                    <!-- / experience -->

                                    <!-- experience -->
                                    <div class="col-md-4 col-6">
                                        <div class="e-content">
                                            <a href="#">
                                                <img src="http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/ec-2.jpg" alt="Image" style="max-width:100%; width: 250px;">
                                            </a>
                                        </div>
                                    </div>
                                    <!-- / experience -->

                                    <!-- experience -->
                                    <div class="col-md-4 col-6">
                                        <div class="e-content">
                                            <a href="#">
                                                <img src="http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/ec-1.jpg" alt="Image" style="max-width:100%; width: 250px;">
                                            </a>
                                        </div>
                                    </div>
                                    <!-- / experience -->

                                </div>
                                <!--.row-->
                            </div>
                        <!--.carousel-inner-->

                    </div>
                </div>

            </div>
            <!-- / Experience Tree -->
            
        </div>
    </section>


<section class="section-padding grey-bg">
    <div class="container">
        <!-- Section title -->
        <div class="section-title">
            THE IDEA!
        </div>
        <!-- / Section title -->

        <!-- Section sub title -->
        <div class="section-sub-title">
        </div>
        <!-- / Section sub title -->

        <div class="col-md-9 mx-auto">
            <p>
                With the Student Tech Experience centre, We want every student to not only realize  his potential, but fully equipped to alter existing realities and create extraordinary things anywhere in the world.

                We are preparing learners to become world class problem solvers and active players in shaping the digital world.
                Teachers can also collaborate and work in a stimulating and resourceful environment

            </p>
        </div>
    </div>
</section>

<?php get_footer(); ?>
