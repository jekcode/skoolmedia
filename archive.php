<?php get_header(); ?>

<?php 

$getimage = tr_posts_field('banner_image');
$image = wp_get_attachment_image_url($getimage);

$background_image  = !empty( $image ) ?  $image : 'http://sm.test/wp-content/uploads/2018/11/patrick-tomasso-71909-unsplash.jpg';
$heading = !empty( tr_posts_field('heading') ) ? tr_posts_field('heading') : 'This is Skool Media';
$subheading = !empty( tr_posts_field('subheading') ) ? tr_posts_field('subheading') : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at auctor turpis, ac feugiat sapien. Maecenas auctor urna egestas, placerat felis a, ultrices dui. Nam quis convallis ex, eu pellentesque diam. Praesent non lacinia risus. Donec pharetra, ipsum non eleifend posuere, lectus nisi suscipit urna, in convallis lectus erat vitae metus. Integer pharetra et enim sed dapibus. Mauris semper purus ipsum, a accumsan tortor molestie sit amet.';
$color_tint = !empty( tr_posts_field('color_tint') ) ? tr_posts_field('color_tint') : 'green';

?>

    <!-- Hero Text Intro -->
    <section class="hero-basic" style="background-image: url('<?php echo $background_image ; ?>">
        <div class="w-100 <?php echo $color_tint; ?>-tint">
            <div class="col-lg-6 mx-auto">
                <div class="hero-info">

                    <!-- Careers Page Title -->
                    <div class="title">
                        <h1>
                            <?php echo $heading; ?>
                        </h1>
                    </div>
                    <!-- / Careers Page Title -->

                    <!-- Page Excerpt -->
                    <div class="copy">
                        <p>
                            <?php echo $subheading; ?>
                        </p>
                    </div>
                    <!-- / Careers Page Excerpt -->

                </div>
            </div>
        </div>
    </section>
    <!-- / Hero Text Intro -->

	<!-- Top News -->
    <section class="section-padding-news">
        <div class="container">

            <div class="section-title">
                <?php _e( 'Top News from ', 'skoolmedia' ); single_cat_title(); ?>
            </div>

            <div class="section-sub-title">
            </div>

            <div class="row news-row">

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

            </div>
        </div>
    </section>
    <!-- / Top News -->

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
