<?php // single-tr_team.php
get_header(); ?>
 <?php while (have_posts()) : the_post() ?>
        
 <!-- Hero Image Intro -->
 <section class="job-hero-img" style="background-image: url('assets/img/marten-bjork-707746-unsplash.jpg');">
        <div class="w-100 black-tint">
            <div class="display-tr">

            </div>
        </div>
    </section>
    <!-- / Hero Image Intro -->

    <section class="grey-bg section-padding">
        <div class="container">

            <!-- Floating Intro Box -->
            <div class="col-lg-8 mx-auto">
                <div class="floating-box">
                    <div class="section-title">
                        <p>
                        <?php the_title(); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- / Floating Intro Box -->

            <!-- Contact Form -->
            <div class="col-lg-7 mx-auto">
                <div class="contact-box">
                    <!-- Contact Title -->
                    <div class="title">
                        Fill the form below
                    </div>
                    <!-- / Contact Title -->

                    <!-- Job Application Form Boxes -->
                    <div>
                    <?php echo do_shortcode('[contact-form-7 id="569" title="Application Form"]'); ?>
                    </div>
                    <!-- / Job Application Form Boxes -->
                </div>
            </div>
            <!-- / Job Application Form -->

        </div>
    </section>

    <?php endwhile; ?>
</section>
<?php get_footer(); ?>