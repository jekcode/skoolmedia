    <!-- Footer -->
    <footer class="section-padding-footer">
        <div class="container">

            <!-- Footer Box row -->
            <div class="row">

                <!-- Footer Box 1 -->
                <div class="col-6 col-md-3">
                    <div class="ft-links">

                        <?php
                        if(is_active_sidebar('footer-column-1')){
                        dynamic_sidebar('footer-column-1');
                        }
                            ?>


                    </div>
                </div>
                <!-- / Footer Box 1 -->

                <!-- Footer Box 2 -->
                <div class="col-6 col-md-3">
                    <div class="ft-links">

                         <?php
                        if(is_active_sidebar('footer-column-2')){
                        dynamic_sidebar('footer-column-2');
                        }
                            ?>

                    </div>
                </div>
                <!-- / Footer Box 2 -->

                <!-- Footer Box 3 -->
                <div class="col-6 col-md-3">
                    <div class="ft-links">

                        <?php
                        if(is_active_sidebar('footer-column-3')){
                        dynamic_sidebar('footer-column-3');
                        }
                            ?>

                    </div>
                </div>
                <!-- / Footer Box 3 -->

                <!-- Footer Box 4 -->
                <div class="col-6 col-md-3">
                    <div class="ft-links">

                        <!-- Footer Social link -->
                        <li>
                            <a class="social-link" target="_blank" href="<?php echo tr_options_field('tr_theme_options.facebook'); ?>">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_options_field('tr_theme_options.instagram'); ?>">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_options_field('tr_theme_options.twitter'); ?>">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_options_field('tr_theme_options.youtube'); ?>">
                                <i class="fa fa-youtube"></i>
                            </a>

                             <a class="social-link" target="_blank" href="<?php echo tr_options_field('tr_theme_options.linkedin'); ?>">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <!-- / Footer Social link -->

                    </div>
                </div>
                <!-- / Footer Box 4 -->

            </div>
            <!-- / Footer Box row -->

            <div class="copyright">
                Copyright &copy;
                <script type="text/javascript">
                    document.write(new Date().getFullYear());
                </script> Skool Media
            </div>

        </div>
    </footer>
    <!-- / Footer -->

    

    <!-- ----------------------------------------------------------------------------------------------------------------------------- -->

    <!-- ----------------------------------------------------------------------------------------------------------------------------- -->

    <!-- Search Modal -->
    <div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog animated fadeIn modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="search-form" id="search-form" class="needs-validation" novalidate>
                        <div class="col-lg-6 mx-auto">
                            <div class="input-group autocomplete">
                                <form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
                                    <input id="search" class="form-control" type="search" placeholder="Looking for something? Type here..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Looking for something? Type here...'" autocomplete="off" required name="s" />
                                    <div class="input-group-append">
                                        <span class="input-group-text"><button class="btn" type="submit"><i class="fa fa-search"></i></button></span>
                                    </div>
                                    <div class="invalid-feedback">
                                        The search field is empty. Please input a value.
                                    </div>
                                </form>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- / Search Modal -->
    
    <!-- Subscribe Modal -->
    <div class="modal fade" id="subscribe-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog animated fadeIn modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo do_shortcode('[mc4wp_form id="579"]'); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- / Subscribe Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>

    <script type="text/javascript">
        $('[data-toggle="datepicker"]').datepicker();
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
    

    <?php wp_footer(); ?>

		<!-- analytics -->
		<script>
			(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
			(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
			l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
			ga('send', 'pageview');
		</script>

    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true,
                wrapping: false
            });
        });
    </script>

    <script type="text/javascript">
        $(function() {
            var selectedClass = "";
            $(".fil-cat").click(function(){ 
            selectedClass = $(this).attr("data-rel"); 
         $("#portfolio").fadeTo(100, 0.1);
            $("#portfolio div").not("."+selectedClass).fadeOut().removeClass('scale-anm');
        setTimeout(function() {
          $("."+selectedClass).fadeIn().addClass('scale-anm');
          $("#portfolio").fadeTo(300, 1);
        }, 300); 
            
        });
    });
    </script>
    
    <script type="text/javascript">
$('#tt-logo').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});
</script>
    

</body>

</html>

