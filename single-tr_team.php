<?php // single-tr_team.php
get_header(); ?>
<section id="content">
    <?php while (have_posts()) : the_post() ?>
        <h1><?php the_title(); ?></h1>
        <section>
            <p><?php echo tr_posts_field("job_title"); ?></p>
            <?php
            echo wp_get_attachment_image(tr_posts_field("photo"));
            the_content();
            echo get_the_term_list( get_the_ID(), 'department', 'Department: ', ', ', '' );
            ?>
        </section>
    <?php endwhile; ?>
</section>
<?php get_footer(); ?>