<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<meta property="og:locale" content="en_US" />
	    <meta property="og:type" content="website" />
	    <meta property="og:title" content="Skool Media | The first Edutech Solutions Provider across Africa" />
	    <meta property="og:site_name" content="Skool Media | The first Edutech Solutions Provider across Africa" />
	    <meta name="twitter:card" content="summary" />
	    <meta name="twitter:title" content="Skool Media | The first Edutech Solutions Provider across Africa" />

	     <link href="https://fonts.googleapis.com/css?family=Arimo:400,700" rel="stylesheet">
	    <link href="https://fonts.googleapis.com/css?family=Oswald:300,700" rel="stylesheet">
	    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
	    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	    
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body>

		<button class="back-to-top" type="button"></button>
    <!-- Navigation -->
    <header>
        <div class="container">
            <nav id="navbar" class="navbar navbar-expand-xl">
                <a class="navbar-brand" href="<?php echo home_url(); ?>">
                    <img src="http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/Skool-Media-PNG-2.png" class="img-fluid" />
                </a>

                <!-- Navigation Links -->
                <button class="navbar-toggler offcanvas-toggle" type="button" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>

                <div class="navbar-collapse navbar-offcanvas navbar-offcanvas-touch" id="js-bootstrap-offcanvas">

                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'header-menu',
                            'menu_id'        => 'primary-menu',
                            'container'      => false,
                            'depth'          => 2,
                            'menu_class'     => 'navbar-nav ml-auto',
                            'walker'         => new Bootstrap_NavWalker(),
                            'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
                        ) ); ?>
                    
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item nav-icon">
                            <a class="nav-link search" href="javascript:void(0)" data-toggle="modal" data-target="#search-modal">
                                <i class="fa fa-2x fa-search"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <!-- / Navigation Links -->
            </nav>
        </div>
    </header>

    <!-- Fixed Social Icons -->
    <div style="position: fixed; right: 0; top: 0; z-index: 3;">
    <div id="sideSocial" class="d-none d-md-block">
        <a href="<?php echo tr_options_field('tr_theme_options.twitter'); ?>" target="_blank" id="twitter" title="Follow on twitter"><i class="fa fa-twitter"></i></a>
        <a href="<?php echo tr_options_field('tr_theme_options.facebook'); ?>" target="_blank" id="facebook" title="Follow on facebook"><i class="fa fa-facebook"></i></a>
        <a href="<?php echo tr_options_field('tr_theme_options.instagram'); ?>" target="_blank" id="instagram" title="Follow on instagram"><i class="fa fa-instagram"></i></a>
        <a href="<?php echo tr_options_field('tr_theme_options.linkedin'); ?>" target="_blank" id="linkedin" title="Follow on linkedin"><i class="fa fa-linkedin"></i></a>
    </div>
    </div>
    <!-- / Fixed Social Icons -->
