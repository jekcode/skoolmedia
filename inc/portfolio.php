<?php // functions.php

$portfolio = tr_post_type('School', 'Portfolio');
$portfolio->setId('tr_portfolio');
$portfolio->setIcon('suitcase');
$portfolio->setArgument('supports', ['title'] );
tr_meta_box('School Details')->apply($portfolio);
tr_taxonomy('Group')->apply($portfolio);

/*
function add_meta_content_portfolio_details() {
    $form = tr_form();
    echo $form->text('School Address');
    echo $form->text('School Email');
    echo $form->text('School Phone');
}*/

function add_meta_content_school_details() {
	 $form = tr_form();
    echo $form->text('School Address');
    echo $form->text('School Email');
    echo $form->text('School Phone');
}

$portfolio->setTitlePlaceholder( 'Enter school name here' );

$portfolio->setTitleForm( function() {
    $form = tr_form();
    echo $form->image('School Logo');
    $editor = $form->editor('post_content');
    echo $editor->setLabel('About Partner School');
} );