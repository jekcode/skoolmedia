<?php // functions.php

$vacancies = tr_post_type('vacancies', 'vacancies');
$vacancies->setId('tr_vacancies');
$vacancies->setIcon('tablet');
$vacancies->setArgument('supports', ['title'] );
tr_meta_box('Job Details')->apply($vacancies);

function add_meta_content_job_details() {
    //echo $form->date('Event Date');
    $form = tr_form();
    //echo $form->date('Event Date');
    echo $form->date('Closing date: ');
    
}

$vacancies->setTitlePlaceholder( 'Enter job title' );

$vacancies->setTitleForm( function() {
    $form = tr_form();
    //echo $form->image('Event Image');
    $editor = $form->editor('post_content');
    echo $editor->setLabel('Job Description');
} );