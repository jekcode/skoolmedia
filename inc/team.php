<?php // functions.php

$team = tr_post_type('Team Member', 'Team');
$team->setId('tr_team');
$team->setIcon('users');
$team->setArgument('supports', ['title'] );
tr_meta_box('Team Details')->apply($team);
tr_taxonomy('Level')->apply($team);

function add_meta_content_team_details() {
    $form = tr_form();
    echo $form->text('Job Title');
    echo $form->text('Branch Location');
    echo $form->text('Facebook');
    echo $form->text('Instgram');
    echo $form->text('Twitter');
    echo $form->text('Youtube');
}

$team->setTitlePlaceholder( 'Enter full name here' );

$team->setTitleForm( function() {
    $form = tr_form();
    echo $form->image('Photo');
    $editor = $form->editor('post_content');
    echo $editor->setLabel('About Team Member');
} );