<?php // functions.php

$experience = tr_post_type('Experience', 'Experience');
$experience->setId('tr_experience');
$experience->setIcon('tablet');
$experience->setArgument('supports', ['title'] );

tr_meta_box('Experience Details')->apply($experience);

function add_meta_content_experience_details() {
	 $form = tr_form();
	  $options = [
        'Orange' => 'orange',
        'Green' => 'green',
    ];
    echo $form->select('Color')->setOptions($options);
	echo $form->repeater('Experience')->setFields([
        $form->image('Photo'),
        $form->text('Name'),
    ]);

}

$experience->setTitlePlaceholder( 'Enter experinece details' );

$experience->setTitleForm( function() {
    $form = tr_form();
    echo $form->image('Featured Image');
    $editor = $form->editor('post_content');
    echo $editor->setLabel('About Experience');
} );