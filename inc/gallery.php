<?php // functions.php

$gallery = tr_post_type('gallery', 'gallery');
$gallery->setId('tr_gallery');
$gallery->setIcon('tablet');
$gallery->setArgument('supports', ['title'] );
//tr_taxonomy('Event Category')->apply($gallery);

tr_meta_box('Gallery Details')->apply($gallery);

function add_meta_content_gallery_details() {
    //echo $form->date('Event Date');
    $form = tr_form();
    echo $form->gallery('Gallery');
    echo $form->text('gallery-slug');
    //$gallery->setSetting('button', 'Insert Images');
}

$gallery->setTitlePlaceholder( 'Enter gallery title' );

$gallery->setTitleForm( function() {
    $form = tr_form();
    echo $form->image('Gallery Featured Image');
    $editor = $form->editor('post_content');
    echo $editor->setLabel('About gallery');
} );