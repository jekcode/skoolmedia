<?php // functions.php

$competition = tr_post_type('Competition', 'Competition');
$competition->setId('tr_competition');
$competition->setIcon('tablet');
$competition->setArgument('supports', ['title'] );
tr_taxonomy('Event Category')->apply($competition);

tr_meta_box('Competition Details')->apply($competition);

function add_meta_content_competition_details() {
    //echo $form->date('Event Date');
    $form = tr_form();
    echo $form->date('Event Date');
    echo $form->text('Focus');
    echo $form->text('Time');
    echo $form->text('Venue');
    echo $form->gallery('Gallery');
    //$gallery->setSetting('button', 'Insert Images');
}

$competition->setTitlePlaceholder( 'Enter experinece details' );

$competition->setTitleForm( function() {
    $form = tr_form();
    echo $form->image('Event Image');
    $editor = $form->editor('post_content');
    echo $editor->setLabel('About Competition');
} );