<?php // functions.php

$awards = tr_post_type('awards', 'awards');
$awards->setId('tr_awards');
$awards->setIcon('tablet');
$awards->setArgument('supports', ['title'] );
tr_taxonomy('Event Category')->apply($awards);
tr_meta_box('awards Details')->apply($awards);

function add_meta_content_awards_details() {
    //echo $form->date('Event Date');
    $form = tr_form();
    echo $form->date('Event Date');
    echo $form->text('Focus');
    echo $form->text('Time');
    echo $form->text('Venue');
    echo $form->gallery('Gallery');
    //$gallery->setSetting('button', 'Insert Images');
}

$awards->setTitlePlaceholder( 'Enter experinece details' );

$awards->setTitleForm( function() {
    $form = tr_form();
    echo $form->image('Event Image');
    $editor = $form->editor('post_content');
    echo $editor->setLabel('About awards');
} );