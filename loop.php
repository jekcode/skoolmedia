<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<div class="col-md-4">
                            <!-- News Box -->
                            <div class="news-box">

                                <!-- News Featured Image -->
                                <div class="news-feature-image" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/jessica-ruscello-196422-unsplash.jpg');">

                                    <!-- News Date -->
                                    <div class="news-date">
                                        <p class="day"><?php the_time('d'); ?></p>
                                        <p class="month"><?php the_time('M'); ?></p>
                                    </div>
                                    <!-- / News Date -->

                                    <!-- News Title -->
                                    <!--Important! - Remove 'black-tint' if there is no featured image -->
                                    <div class="display-tr black-tint">
                                        <div class="news-title">
                                            <?php the_title(); ?>
                                        </div>
                                    </div>
                                    <!-- / News Title -->

                                </div>
                                <!-- / News Featured Image -->

                                <!-- News Excerpt -->
                                <div class="news-excerpt">
                                    <?php //the_excerpt(20); ?>
                                    <?php html5wp_excerpt(); ?>
                                </div>
                                <!-- / News Excerpt -->

                                <!-- News link -->
                                <div class="news-link">
                                    <a href="<?php the_permalink(); ?>">
                                            Read more
                                        </a>
                                </div>
                                <!-- / News link -->

                            </div>
                            <!-- / News Box -->
                        </div>

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
