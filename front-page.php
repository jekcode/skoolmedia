<?php /* Template Name: Home Page Template */ get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

<?php the_content(); ?>

<?php //comments_template( '', true ); // Remove if you don't want comments ?>

<?php edit_post_link(); ?>

<?php endwhile; ?>

<?php else: ?>

<h2>
    <?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?>
</h2>

<?php endif; ?>

<!-- Top News -->
<section class="section-padding-news">
    <div class="container">

        <div class="section-title">
            Top News
        </div>

        <div class="section-sub-title">
        </div>

        <div class="container">
            <div class="row news-row">


                <?php 

                $args = array(
                    'post_type'      => 'post',
                    //'cat'            => '22,47,67',
                    'orderby'        => 'date',
                    'order'          => 'DESC',
                    'hide_empty'     => 1,
                    //'depth'          => 1,
                    'posts_per_page' => 3
                );

                // the query
                $the_query = new WP_Query( $args ); ?>

                <?php if ( $the_query->have_posts() ) : ?>

                <!-- pagination here -->

                <!-- the loop -->
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                <div class="col-md-4">
                    <!-- News Box -->
                    <div class="news-box">

                        <!-- News Featured Image -->
                        <div class="news-feature-image" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/jessica-ruscello-196422-unsplash.jpg');">

                            <!-- News Date -->
                            <div class="news-date">
                                <p class="day">
                                    <?php the_time('d'); ?>
                                </p>
                                <p class="month">
                                    <?php the_time('M'); ?>
                                </p>
                            </div>
                            <!-- / News Date -->

                            <!-- News Title -->
                            <!--Important! - Remove 'black-tint' if there is no featured image -->
                            <div class="display-tr black-tint">
                                <div class="news-title">
                                    <?php the_title(); ?>
                                </div>
                            </div>
                            <!-- / News Title -->

                        </div>
                        <!-- / News Featured Image -->

                        <!-- News Excerpt -->
                        <div class="news-excerpt">
                            <?php //the_excerpt(20); ?>
                            <?php html5wp_excerpt(); ?>
                        </div>
                        <!-- / News Excerpt -->

                        <!-- News link -->
                        <div class="news-link">
                            <a href="<?php the_permalink(); ?>">
                                Read more
                            </a>
                        </div>
                        <!-- / News link -->

                    </div>
                    <!-- / News Box -->
                </div>
                <?php endwhile; ?>
                <!-- end of the loop -->

                <!-- pagination here -->

                <?php wp_reset_postdata(); ?>

                <?php else : ?>
                <p>
                    <?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?>
                </p>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<!-- / Top News -->

<!-- Tested and trusted -->
<section class="grey-bg section-padding">
    <div class="container">

        <!-- Section title -->
        <div class="section-title">
            Tested and Trusted
        </div>

        <div class="section-sub-title">
            We’ve worked with countless schools and we’ve delivered on our promise
        </div>
        <!-- /Section title -->

        <div class="col-lg-7 mx-auto">
            <div class="testimonial-row">
                <div id="tt-logo" class="carousel slide" data-ride="carousel" data-pause="hover" data-interval="5000">
                    <div class="carousel-inner w-100 row mx-auto">


                        <?php 

                $args = array(
                    'post_type'      => 'tr_portfolio',
                    //'cat'            => '22,47,67',
                    'orderby'        => 'date',
                    'order'          => 'ASC',
                    'hide_empty'     => 1,
                    //'depth'          => 1,
                    'posts_per_page' => -1
                );

                // the query
                $the_query = new WP_Query( $args );?>

                        <?php if ( $the_query->have_posts() ) : ?>

                        <?php //var_dump($the_query); ?>

                        <!-- pagination here -->

                        <?php $count = 0; ?>
                        <!-- the loop -->
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $count++; ?>

                        <div class="carousel-item col-4 <?php if ($count == 1) {echo 'active';} ?>">
                            <!-- Testimonial Logo -->
                                <div class="testimonial-logo">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo wp_get_attachment_image_url(tr_posts_field('school_logo')); ?>">
                                </div>
                            <!-- / Testimonial Logo -->
                        </div>

                        <?php endwhile; ?>
                        <!-- end of the loop -->

                        <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                        <p>
                            <?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?>
                        </p>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="btn-holder">
            <a class="btn btn-red-primary" href="/schools">
                Begin our Journey
            </a>
        </div>

    </div>
</section>
<!-- /Tested and trusted -->

<?php get_footer(); ?>