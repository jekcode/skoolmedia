<?php

// Before VC Init
add_action( 'vc_before_init', 'vc_before_init_actions' );
 
function vc_before_init_actions() {
 
    // Including the separate file created here
    require_once( get_template_directory().'/vc-elements/home-top-banner.php' ); 
     // Including the separate file created here
    require_once( get_template_directory().'/vc-elements/floating-box.php' ); 
    require_once( get_template_directory().'/vc-elements/floating-box-v2.php' ); 
    require_once( get_template_directory().'/vc-elements/home-service-box.php' );
    require_once( get_template_directory().'/vc-elements/counter-config.php' );
    require_once( get_template_directory().'/vc-elements/footer-top-box.php' );
    require_once( get_template_directory().'/vc-elements/solutions-box.php' );
    require_once( get_template_directory().'/vc-elements/award-config.php' );
    require_once( get_template_directory().'/vc-elements/benefit-box.php' );
}

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.