<?php /* Template Name: Contact Page Template */ get_header(); ?>
<!-- Hero Image Intro -->
    <section class="hero-map" style="background-image: url('http://wordpress-211013-658479.cloudwaysapps.com/wp-content/uploads/2018/11/map.png');">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.488065011037!2d3.40493731536436!3d6.4596782953267455!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8b387b8b344d%3A0x3c62d75795f100dd!2s14+Royal+Palm+Dr%2C+Ikoyi%2C+Lagos!5e0!3m2!1sen!2sng!4v1543565392327" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </section>
    <!-- / Hero Image Intro -->

    <!-- Floating contact Box -->
    <section class="section-padding grey-bg">
        <div class="container">

            <!-- Floating Intro Box -->
            <!-- Floating Intro Box -->
            <div class="col-lg-8 mx-auto">
                <div class="floating-box">
                    <div class="section-title">
                        <p>
                            Reach us with ease
                        </p>
                    </div>

                    <div class="section-sub-title">
                    </div>

                    <!-- Contact Information -->
                    <div class="row">

                        <!-- Contact Box -->
                        <div class="col-12 col-md-4">

                            <div class="contact">
                                <!-- Contact Icon -->
                                <div class="icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <!-- / Contact Icon -->

                                <!-- Contact Heading -->
                                <div class="heading">
                                    Head Office:
                                </div>
                                <!-- / Contact Heading -->

                                <!-- Contact Description -->
                                <div class="description">
                                <?php echo tr_options_field('tr_theme_options.head_office_address'); ?>
                                   
                                </div>
                                <!-- / Contact Description -->
                            </div>
                        </div>
                        <!-- / Contact Box -->

                        <!-- Contact Box -->
                        <div class="col-12 col-md-4">

                            <div class="contact">
                                <!-- Contact Icon -->
                                <div class="icon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <!-- / Contact Icon -->

                                <!-- Contact Heading -->
                                <div class="heading">
                                    Email Address:
                                </div>
                                <!-- / Contact Heading -->

                                <!-- Contact Description -->
                                <div class="description">
                                    <?php echo tr_options_field('tr_theme_options.company_email'); ?>
                                </div>
                                <!-- / Contact Description -->
                            </div>
                        </div>
                        <!-- / Contact Box -->

                        <!-- Contact Box -->
                        <div class="col-12 col-md-4">

                            <div class="contact">
                                <!-- Contact Icon -->
                                <div class="icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <!-- / Contact Icon -->

                                <!-- Contact Heading -->
                                <div class="heading">
                                    Phone Number:
                                </div>
                                <!-- / Contact Heading -->

                                <!-- Contact Description -->
                                <div class="description">
                                <?php echo tr_options_field('tr_theme_options.company_phone'); ?>
                                </div>
                                <!-- / Contact Description -->
                            </div>
                        </div>
                        <!-- / Contact Box -->

                        <div class="w-100"></div>

                </div>
            </div>
            <!-- / Floating Intro Box -->

            <!-- Contact Form -->
            <div class="col-lg-7 mx-auto">
                <div class="contact-box">
                    <!-- Contact Title -->
                    <div class="title">
                        Send a message
                    </div>
                    <!-- / Contact Title -->

                    <!-- Contact Form Boxes -->
                    <div>
                    <?php echo do_shortcode('[contact-form-7 id="398" title="Contact Form"]'); ?>
                    </div>
                    <!-- / Contact Form Boxes -->
                </div>
            </div>
            <!-- / Contact Form -->

        </div>
    </section>
    <!-- / News -->
    

<?php get_footer(); ?>
