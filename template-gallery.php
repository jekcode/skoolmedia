<?php /* Template Name: Gallery Page Template */ get_header(); ?>
<?php 

$getimage = tr_posts_field('banner_image');
$image = wp_get_attachment_image_url($getimage);

$background_image  = !empty( $image ) ?  $image : 'http://sm.test/wp-content/uploads/2018/11/patrick-tomasso-71909-unsplash.jpg';
$heading = !empty( tr_posts_field('heading') ) ? tr_posts_field('heading') : 'This is Skool Media';
$subheading = !empty( tr_posts_field('subheading') ) ? tr_posts_field('subheading') : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at auctor turpis, ac feugiat sapien. Maecenas auctor urna egestas, placerat felis a, ultrices dui. Nam quis convallis ex, eu pellentesque diam. Praesent non lacinia risus. Donec pharetra, ipsum non eleifend posuere, lectus nisi suscipit urna, in convallis lectus erat vitae metus. Integer pharetra et enim sed dapibus. Mauris semper purus ipsum, a accumsan tortor molestie sit amet.';
$color_tint = !empty( tr_posts_field('color_tint') ) ? tr_posts_field('color_tint') : 'green';

?>
<!-- Gallery -->
    <section class="grey-bg section-padding-stats">
        <div class="container">

            <!-- Gallery Header -->
            <div class="row">

                <div class="col-3 mr-auto">
                    <h4>
                        Gallery
                    </h4>
                </div>
                    <!-- / Filter Gallery -->
                </div>

            </div>
            <!-- / Gallery Header -->

            <!-- Gallery Content -->
            <div class="container shadow-sm bg-white">
                <div class="gallery-content-holder">
                    <div class="row image-gallery">

                        <!-- Gallery Box with Light Box View -->

                        <?php 

                                $args = array(
                                    'post_type'      => 'tr_gallery',
                                    //'cat'            => '22,47,67',
                                    'orderby'        => 'date',
                                    'order'          => 'DESC',
                                    //'hide_empty'     => 1,
                                    //'depth'          => 1,
                                    'posts_per_page' => -1
                                );

                                // the query
                                $the_query = new WP_Query( $args ); ?>

                                <?php if ( $the_query->have_posts() ) : $counter = 0;?>
                                   
                                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $counter++;?>

                                    <!-- Gallery Image -->
                                    <div class="col-6 col-md-3 mb-3">

                                        <a href="<?php echo wp_get_attachment_image_url(tr_posts_field('gallery_featured_image')); ?>" data-toggle="lightbox" data-gallery="<?php echo tr_posts_field('gallery_slug'); ?>" data-title="<?php the_title(); ?>">  
                                                <img src="<?php echo wp_get_attachment_image_url(tr_posts_field('gallery_featured_image')); ?>" class="img-fluid">

                                            <!-- Image Details -->
                                            <div class="bg-white p-2">
                                                <div class="gallery-image-title">
                                                    <?php the_title(); ?>
                                                </div>

                                                <div class="gallery-image-desc">
                                                    <?php the_content(); ?>
                                                </div>
                                            </div>
                                            <!-- / Image Details -->
                                            
                                        </a>

                                        <?php 
                                            $images = tr_posts_field('gallery'); 

                                            foreach ( $images as  $image) {
                                                $slug = tr_posts_field('gallery_slug');
                                                //echo "$value <br>";
                                                echo '<div data-toggle="lightbox" data-gallery="'.$slug.'" data-remote="'.wp_get_attachment_image_url($image).'" data-title="">
                                                </div>';
                                            }
                                        ?>
                                        
                                <!-- / Gallery Image -->

                                </div>

                                 <?php endwhile; ?>
                    
                                    <!-- end of the loop -->
                                    <?php wp_reset_postdata(); ?>

                                    <?php else : ?>
                                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                                    <?php endif; ?>


                        <!-- / Gallery Box with Light Box View -->

                    </div>
                </div>
            </div>
            <!-- / Gallery Content -->

        
    </section>
    <!-- / Gallery -->

<?php get_footer(); ?>


    
