<?php /* Template Name: Competition Page Template */ get_header(); ?>
<?php 

$getimage = tr_posts_field('banner_image');
$image = wp_get_attachment_image_url($getimage);

$background_image  = !empty( $image ) ?  $image : 'http://sm.test/wp-content/uploads/2018/11/patrick-tomasso-71909-unsplash.jpg';
$heading = !empty( tr_posts_field('heading') ) ? tr_posts_field('heading') : 'This is Skool Media';
$subheading = !empty( tr_posts_field('subheading') ) ? tr_posts_field('subheading') : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at auctor turpis, ac feugiat sapien. Maecenas auctor urna egestas, placerat felis a, ultrices dui. Nam quis convallis ex, eu pellentesque diam. Praesent non lacinia risus. Donec pharetra, ipsum non eleifend posuere, lectus nisi suscipit urna, in convallis lectus erat vitae metus. Integer pharetra et enim sed dapibus. Mauris semper purus ipsum, a accumsan tortor molestie sit amet.';
$color_tint = !empty( tr_posts_field('color_tint') ) ? tr_posts_field('color_tint') : 'green';

?>

    <!-- Hero Text Intro -->
    <section class="hero-basic" style="background-image: url('<?php echo $background_image ; ?>">
        <div class="w-100 <?php echo $color_tint; ?>-tint">
            <div class="col-lg-6 mx-auto">
                <div class="hero-info">

                    <!-- Careers Page Title -->
                    <div class="title">
                        <h1>
                            <?php echo $heading; ?>
                        </h1>
                    </div>
                    <!-- / Careers Page Title -->

                    <!-- Page Excerpt -->
                    <div class="copy">
                        <p>
                            <?php echo $subheading; ?>
                        </p>
                    </div>
                    <!-- / Careers Page Excerpt -->

                </div>
            </div>
        </div>
    </section>
    <!-- / Hero Text Intro -->

 <!-- Statistics -->
    <section class="section-padding-stats grey-bg">
        <div class="container">
            <div class="col-lg-9 mx-auto">
                <?php
                    $args = array(
                      'orderby' => 'ID'
                    );
                    $terms = get_terms( 'event_category', $args );
                    ?>
                <!-- Nav tabs -->
                <nav class="competitions">
                    <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">

                        <?php
                      $count = 0;
                      foreach ( $terms as $term ) : $count ++; ?>
                        <a class="nav-item nav-link <?php if ( $count == 1 ) echo ' active' ?>" data-toggle="tab" href="#<?php echo $term->slug ?>" role="tab" aria-selected="true"><?php echo $term->name ?></a>
                        <?php
                      endforeach; ?>

                    </div>
                </nav>


            </div>
        </div>
    </section>
    <!-- / Statistics -->

    <section class="section-padding">
        <div class="container">
            <div class="col-lg-9 mx-auto comp-details">
                <!-- Tab panes -->
                <div class="tab-content">

                    <?php
                      $count = 0;
                      foreach ( $terms as $term ) : $count ++; ?>
                        <div class="tab-pane <?php if ( $count == 1 ) echo 'active' ?>" id="<?php echo $term->slug ?>" role="tabpanel" aria-labelledby="home-tab">
                          <?php
                          $args = array(
                            'post_type' => 'tr_competition',
                            'tax_query' => array(
                              array(
                                'taxonomy' => $term->taxonomy,
                                'field'    => $term->slug,
                                'terms'    => $term->term_id
                              )
                            )
                          );
                          $loop = new WP_Query( $args );
                          if ( $loop->have_posts() ) :
                            while ( $loop->have_posts() ) : $loop->the_post(); ?>

                            <!-- Competition -->
                            <div class="col-6 col-md-4">
                                <!-- Competition Box -->
                                <div class="comp-box">
                                    <div class="comp-content">
                                        <div class="comp-info">
                                            <!-- Name of the Competition -->
                                            <div class="comp-title">
                                                <?php the_title(); ?>
                                            </div>
                                            <!-- / Name of the Competition -->

                                            <!-- Date of the Competition -->
                                            <div class="comp-tag">
                                                Date: <?php tr_posts_field('event_date'); ?>
                                            </div>
                                            <!-- / Date of the Competition -->

                                            <!-- View more gallery of the Competition -->
                                            <div class="comp-view">
                                                <a class="tip" href="#" data-placement="top" title="View Gallery"><i class="fa fa-plus-square-o"></i></a>
                                            </div>
                                            <!-- View more gallery of the Competition -->
                                        </div>
                                        <!-- Image of the Competition. Must be landscape -->
                                        <div class="comp-image">
                                            <img class="img-fluid" src="<?php $getimage = tr_posts_field('event_image'); $image = wp_get_attachment_image_url($getimage); echo $image; ?>">
                                        </div>
                                        <!-- / Image of the Competition -->
                                    </div>
                                </div>
                                <!-- / Competition Box -->
                            
                            <!-- / Competition -->
                            </div>

                            <?php
                endwhile;
              endif; wp_reset_query();
              ?>

                        
                    </div>

                    <?php endforeach; ?>

                    <div class="tab-pane" id="lorem" role="tabpanel">
                    </div>
                    <div class="tab-pane" id="ipsum" role="tabpanel">
                        <!--Fill using the COmpetition ABove -->
                    </div>
                </div>
            </div>
        </div>
    </section>



    

<?php get_footer(); ?>
