<?php
if ( is_admin() ) {
return;
}
extract( vc_map_get_attributes( 'redfish_counter', $atts ) );  
$counter_data = (array) vc_param_group_parse_atts( $counter_data );
?>
<section class="section-padding-stats grey-bg">
        <div class="container">

            <div class="col-md-5 mx-auto">
                <div class="row">
        <?php 
      
      foreach ( $counter_data as $counter ) {
      $counter_number  = !empty( $counter['counter_number'] ) ? $counter['counter_number'] : '1,200';
      $counter_title = !empty( $counter['counter_title'] ) ? $counter['counter_title'] : 'schools';
      $counter_icon = !empty( $counter['counter_icon'] ) ? $counter['counter_icon'] : 'fa fa-users';
      
      ?>
       <!-- Stats Container -->
                    <div class="col">
                        <!-- Stats -->
                        <div class="stats">
                            <div class="icon">
                                <i class="<?php echo esc_html( $counter_icon ); ?>"></i>
                            </div>

                            <div class="main">
                                <?php echo esc_html( $counter_number ); ?>
                            </div>

                            <div class="sub">
                                <?php echo esc_html( $counter_title ); ?>
                            </div>
                        </div>
                        <!-- / Stats -->

                    </div>
                    <!-- / Stats Container -->
      
        <?php } ?>
  

                </div>
            </div>

        </div>
    </section>
