<?php
if ( is_admin() ) {
return;
}
extract( vc_map_get_attributes( 'redfish_solutions', $atts ) );  
$solutions_data = (array) vc_param_group_parse_atts( $solutions_data );
?>
<section class="section-padding-news">
        <div class="container">

            <div class="section-title">
                Distinguished by craft
            </div>

            <div class="section-sub-title">
            </div>

            <!-- Solutions Pane -->
            <div class="col-md-11 mx-auto solutions">
                <div class="solutions">
                    <div class="row">
        <?php
        $args = array(
          'orderby' => 'ID'
        );
        $terms = get_terms( 'testimonial_category', $args );
        ?>

        <!-- bootstrap tabs -->
        <ul class="nav-tabs">
          <?php
          $count = 0;
          foreach ( $terms as $term ) : $count ++; ?>
            <li<?php if ( $count == 1 ) echo ' class="active"' ?>>
              <a href="#<?php echo $term->slug ?>" data-toggle="tab"><?php echo $term->name ?></a>
            </li>
            <?php
          endforeach; ?>
        </ul>

        <div class="tab-content">
          <?php
          $count = 0;
          foreach ( $terms as $term ) : $count ++; ?>
            <div class="tab-pane <?php if ( $count == 1 ) echo 'active' ?>" id="<?php echo $term->slug ?>">
              <?php
              $args = array(
                'post_type' => 'testimonial',
                'tax_query' => array(
                  array(
                    'taxonomy' => $term->taxonomy,
                    'field'    => $term->slug,
                    'terms'    => $term->term_id
                  )
                )
              );
              $loop = new WP_Query( $args );
              if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post(); ?>

                  <blockquote class="testimonial-<?php the_ID(); ?>">
                    <?php the_content(); ?>
                  </blockquote>
                  <cite><?php the_title(); ?></cite>

                  <?php
                endwhile;
              endif; wp_reset_query();
              ?>
            </div><!-- end tab-pane -->
          <?php endforeach; ?>
        </div><!-- end tab-content -->












      <div class="col-md-3">

        <!-- Solution View Controllers -->
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link <?php if($counter == 1 ) { echo 'active' ;} ?>" id="pill-<?php echo $counter; ?>" data-toggle="pill" href="#<?php echo $counter; ?>" role="tab" aria-controls="<?php echo $counter; ?>" aria-selected="true">
              <?php echo esc_html( $solutions_name ); ?></a>
        </div>

        <!-- / Solution View Controllers -->
    </div>

    <div class="col-md-9">
                            <div class="tab-content" id="pills-content">

                                <!-- Solution View -->
                                <div class="tab-pane show fade shadow-sm <?php if ($counter == 1 ) { echo 'active';} ?>" id="<?php echo $counter; ?>" role="tabpanel">
                                    <div class="row">
                                        <!-- Solution Image -->
                                        <div class="col-12 col-md-4">
                                            <div>
                                                <img src="<?php echo esc_html( $solutions_image_url ); ?>');?>" class="img-fluid">
                                            </div>
                                        </div>
                                        <!-- / Solution Image -->

                                        <!-- Solution Text -->
                                        <div class="col-12 col-md-8">
                                            <div>
                                                <p>
                                                    <?php echo esc_html( $solutions_description ); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <!-- / Solution Text -->
                                    </div>
                                </div>
                                <!-- / Solution View -->

              

                            </div>
                        </div>
      <!-- / Feature Box -->
        <?php } ?>
    </div>
          </div>
      </div>
      <!-- / Solutions Pane -->

  </div>
</section>
<!-- / Solutions -->
