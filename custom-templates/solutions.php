<?php
if ( is_admin() ) {
return;
}
extract( vc_map_get_attributes( 'redfish_solutions', $atts ) );  
$solutions_data = (array) vc_param_group_parse_atts( $solutions_data );
?>

<!-- Solutions -->
    <section class="section-padding-news">
        <div class="container">

            <div class="section-title">
                Distinguished by craft
            </div>

            <div class="section-sub-title">
            </div>

            <!-- Solutions Pane -->
            <div class="col-md-11 mx-auto solutions">
                <div class="solutions">
                    <div class="row">

                        <div class="col-md-3">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                          <?php 

                                $count = 0;
                                
                                foreach ( $solutions_data as $solutions ) {
                                $solutions_name  = !empty( $solutions['solutions_name'] ) ? $solutions['solutions_name'] : 'LIBRARY ROOM';
                                $solutions_description = !empty( $solutions['solutions_description'] ) ? $solutions['solutions_description'] : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
                                $solutions_image_url  = !empty( $solutions['solutions_image_url'] ) ? $solutions['solutions_image_url'] : 'http://sm.test/wp-content/uploads/2018/11/neonbrand-426918-unsplash.jpg';
                                $solutions_icon = !empty( $solutions['solutions_icon'] ) ? $solutions['solutions_icon'] : 'fa fa-users';

                                $count++ ; 

                          ?>

                            <!-- Solution View Controllers -->
                            
                                <a class="nav-link <?php if ( $count == 1 ) echo 'active' ?>" id="pill-<?php echo $count; ?>" data-toggle="pill" href="#<?php echo $count; ?>" role="tab" aria-controls="<?php echo $count; ?>" aria-selected="true"><?php echo esc_html( $solutions_name ); ?></a> 
                                <?php } ?>
                            </div>

                            



                            <!-- / Solution View Controllers -->
                        </div>

                        <div class="col-md-9">
                            <div class="tab-content" id="pills-content">

                              <?php 

                                $count = 0;
                                
                                foreach ( $solutions_data as $solutions ) {
                                $solutions_name  = !empty( $solutions['solutions_name'] ) ? $solutions['solutions_name'] : 'LIBRARY ROOM';
                                $solutions_description = !empty( $solutions['solutions_description'] ) ? $solutions['solutions_description'] : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
                                $solutions_image_url  = !empty( $solutions['solutions_image_url'] ) ? $solutions['solutions_image_url'] : 'http://sm.test/wp-content/uploads/2018/11/neonbrand-426918-unsplash.jpg';
                                $solutions_icon = !empty( $solutions['solutions_icon'] ) ? $solutions['solutions_icon'] : 'fa fa-users';

                                $count++ ; 

                          ?>

                                <!-- Solution View -->
                                <div class="tab-pane show fade shadow-sm <?php if ( $count == 1 ) echo 'active' ?>" id="<?php echo $count; ?>" role="tabpanel">
                                    <div class="row">
                                        <!-- Solution Image -->
                                        <div class="col-12 col-md-4">
                                            <div>
                                                <img src="<?php echo esc_html( $solutions_image_url ); ?>" class="img-fluid">
                                            </div>
                                        </div>
                                        <!-- / Solution Image -->

                                        <!-- Solution Text -->
                                        <div class="col-12 col-md-8">
                                            <div>
                                                <p>
                                                    <?php echo esc_html( $solutions_description ); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <!-- / Solution Text -->
                                    </div>
                                </div>
                                <!-- / Solution View -->

                                 <?php } ?>

                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / Solutions Pane -->

        </div>
    </section>
    <!-- / Solutions -->

