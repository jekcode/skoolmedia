<?php

	if ( is_admin() ) {
		return;
	}
		
	extract( vc_map_get_attributes( 'redfish_recent_post', $atts ) );		
	
	$wrapper_classes = array( 'recent-post-section', 'grid-style-post' );	
	
	if( $grid_post_style == 'grid-post-style1' ) {		
		$wrapper_classes[] = 'basic-style';		
		$wrapper_classes[] = 'with-btn';		
	}
	elseif( $grid_post_style == 'grid-post-style2' ) {		
		$wrapper_classes[] = 'basic-style';		
	}
	
	$wrapper_classes = implode( ' ', $wrapper_classes );	
	
	$exclude_post  = explode( ',', $exclude_post );
	
	if( $exclude_categories ) {
		$exclude_categories = explode( ',', $exclude_categories );
	}	
		
	$limit = 8;	
		
	if( $grid_layout == 'vc_col-sm-6 vc_col-md-3' ) {
		$limit = 4;
	}
		
	$args = array(
		'post_type'      => 'post',
		'posts_per_page' => $limit,
		'order'          => $order,
		'orderby'        => $orderby,		
		'post_status'	 => 'publish',
		'post__not_in'   => $exclude_post,
		//'category__not_in' => redfish_get_category_ids( $exclude_categories )		
	);		
		
	$blog_query = new WP_Query( $args );	

	if( $blog_query ->have_posts() ) {
		
?>

<!--Newsroom Section-->
    <section class="newsroom <?php echo esc_attr( $wrapper_classes ); ?>">
        <div class="container">
            <div class="row">
                <div class="border-heading">

                    <h3>NEWSROOM</h3>

                    
                </div>
                <div class="clearfix"></div>

                <div class="gal">

<?php		
		foreach ( $blog_query -> posts as $post ) {				
			
			setup_postdata( $post );		
			
			// Post Vars				
			$post_title		= get_the_title();
			$post_date		= strtoupper( get_the_time( 'F d, Y' ) );			
			$link			= get_permalink();
			$excerpt        = get_the_excerpt();	
			$post_format	= get_post_format();
			$num_comments	= get_comments_number();
			$num_comments	= ( $num_comments > 0 ) ? $num_comments : '';
			$post_format_class	= ( $post_format == '' ) ? '' : 'format-' . $post_format;			
			
			?>


                        <div class="image-grid wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <a href="<?php echo esc_url( $link ); ?>">
                            <img src="<?php the_post_thumbnail_url() ?>">
                                <div class="grid-content">
                                    <h4> <?php echo esc_html( $post_title ); ?></h4>
                                    <p><?php echo wp_kses_post( $excerpt ); ?></p>
                                </div>
                            </a>
                        </div>
         
		<?php		   
		}		
?>		
	</div>


            </div>
        </div>
    </section>

<?php
	}
	else { ?>
		
		<div>
			<h1 class="err-msg"><?php esc_html_e( 'No Post Found, Please Add Post.', 'redfish' ); ?></h1>
		</div>
	<?php 		
	}
	
	wp_reset_postdata();
		
	?>		