<?php
if ( is_admin() ) {
return;
}
extract( vc_map_get_attributes( 'redfish_timeline', $atts ) );  
$timeline_data = (array) vc_param_group_parse_atts( $timeline_data );
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="owl-carousel overview-slider owl-theme">
        <?php 
      $prev_timeline_year = '';
      $index = 1;
      $timeline_class = '';
      foreach ( $timeline_data as $timeline ) {
      $timeline_year  = !empty( $timeline['timeline_year'] ) ? $timeline['timeline_year'] : '2001';
      $timeline_heading = !empty( $timeline['timeline_heading'] ) ? $timeline['timeline_heading'] : 'Start with a Small Services';
      $timeline_date  = !empty( $timeline['timeline_date'] ) ? $timeline['timeline_date'] : '6th Jan 2001';
      $timeline_desc  = !empty( $timeline['timeline_desc'] ) ? $timeline['timeline_desc'] : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed nostrud eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';
      $mod = $index % 2;
      //$count = 0;
      if( $mod == 0 ) {
      $timeline_class = 'first';
      }
      else {
      $timeline_class = 'second';
      }
      ?>
              <?php if ($timeline_class == 'first') {
      ?>
        <div class="item">
          <ul class="overview-yr <?php echo esc_html( $timeline_class  ); ?>">
            <li class="year">
              <?php echo esc_html( $timeline_year ); ?>
            </li>
            <li class="dot-line">
            </li>
            <li class="year-content">
              <?php echo esc_html( $timeline_desc ); ?>
            </li>
          </ul>
        </div>
        <?php } else {
?>
        <div class="item">
          <ul class="overview-yr <?php echo esc_html( $timeline_class  ); ?>">
            <li class="year-content">
              <?php echo esc_html( $timeline_desc ); ?>
            </li>
            <li class="dot-line">
            </li>
            <li class="year">
              <?php echo esc_html( $timeline_year ); ?>
            </li>
          </ul>
        </div>
        <?php } ?>  
        <?php 
$prev_timeline_year = $timeline_year;
$index++;
} 
?>
        </ul>
    </div>  
  </div>
</div>
</div>
</div>
