<?php
if ( is_admin() ) {
return;
}
extract( vc_map_get_attributes( 'redfish_service', $atts ) );  
$service_data = (array) vc_param_group_parse_atts( $service_data );
?>
<section class="section-padding" style="padding-top:0;">
        <div class="container">

<div class="col-lg-12 home-features mx-auto">
   <div class="row">
        <?php 
      
      foreach ( $service_data as $service ) {
      $service_name  = !empty( $service['service_name'] ) ? $service['service_name'] : 'LIBRARY ROOM';
      $service_description = !empty( $service['service_description'] ) ? $service['service_description'] : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
      $service_image_url  = !empty( $service['service_image_url '] ) ? $service['service_image_url '] : 'http://sm.test/wp-content/uploads/2018/11/neonbrand-426918-unsplash.jpg';
      $service_icon = !empty( $service['service_icon'] ) ? $service['service_icon'] : 'fa fa-users';
      
      ?>
      <!-- Feature Box -->
      <div class="col-md-4">
          <div class="feature-image" style="background-image: url('<?php echo esc_html( $service_image_url ); ?>');">
              <div class="black-tint">
                  <div class="feature-title">
                      <span class="icon"><i class="<?php echo esc_html( $service_icon ); ?>"></i></span><?php echo esc_html( $service_name ); ?>
                  </div>

                  <div class="feature-copy">
                      <p>
                          <?php echo esc_html( $service_description ); ?>
                      </p>
                  </div>
              </div>
          </div>
      </div>
      <!-- / Feature Box -->
        <?php } ?>
   </div>
</div>
</div>
</section>
       
