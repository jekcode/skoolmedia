<?php
if ( is_admin() ) {
return;
}
extract( vc_map_get_attributes( 'redfish_location', $atts ) );	
$location_data = (array) vc_param_group_parse_atts( $location_data );
?>
<section class="contact-page">
  <div class="container">
    <div class="row">

<div class="col-xs-12">
        <div class="contact-inner clearfix">
          <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
            <ul>
        <?php	
  			$prev_location_year = '';
  			$index = 1;
  			$location_class = '';

			foreach ( $location_data as $location ) {

			$location_branch	= !empty( $location['location_branch'] ) ? $location['location_branch'] : 'Head Office';
			$location_city	= !empty( $location['location_city'] ) ? $location['location_city'] : 'Lagos';
			$email_address	= !empty( $location['email_address'] ) ? $location['email_address'] : 'invest@fbnquest.com';
      $operating_hours  = !empty( $location['operating_hours'] ) ? $location['operating_hours'] : '8.00 – 17.00 Mondays to Fridays';
      $location_address  = !empty( $location['location_address'] ) ? $location['location_address'] : '18 Keffi Street, Off Awolowo Road S.W.  Ikoyi, Lagos, Nigeria';
			$phone_numbers	= !empty( $location['phone_numbers'] ) ? $location['phone_numbers'] : '+234 810 082 0082, +234 708 065 3100';

			$mod = $index % 2;
			//$count = 0;
			if( $location_branch == 'Head Office') {
			$location_class = 'active';
			}
			else {
			$location_class = '';
			}
			?>
      
              <li class="<?php echo esc_html( $location_class ); ?>">
                <div class="contact-inner-left">
                  <div class="contact-inner-left-heading"><h4><?php echo esc_html( $location_branch ); ?></h4></div>
                  <div class="contact-inner-left-content">

                    <p class="wow fadeInLeft">

                      <strong><?php echo esc_html( $location_city ); ?> </strong> <br>

                      <?php echo esc_html( $location_address ); ?> <br>

                      E-MAIL: <?php echo esc_html( $email_address ); ?> <br>

                      Phone: <?php echo esc_html( $phone_numbers ); ?> <br>

                       Opening Hours:  <?php echo esc_html( $operating_hours ); ?> 

                    </p>


                  </div>
                  
                </div>
              </li>

        <?php 
          $prev_location_year = $location_year;
          $index++;
          } 
          ?>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="details-landing-icon">
              <iframe width="520" height="780" src="https://maps.google.com/maps?width=520&amp;height=780&amp;hl=en&amp;coord=6.444161,3.4133005&amp;q=18%20Keffi%20Street%2C%20Off%20Awolowo%20Road%20S.W.%20Ikoyi%2C%20Lagos+(FBNQuest%20Merchant%20Bank)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"> </iframe>
            </div>
          </div>
        </div>
      </div>
      </div>
  </div>
</section>
