<?php

	if ( is_admin() ) {
		return;
	}
	
	extract( vc_map_get_attributes( $this->getShortcode(), $atts ) );	

	$wrapper_classes = array( 'featured-box' );	
	$wrapper_style = array();
	$icon_style = array();
	$upper_heading_style = array();
	$upper_heading_classes = array();
	$heading_style = array();
	$heading_classes = array();
	$desc_style = array();
	$desc_classes = array();
	$btn_css_style = array();
	$btn_data_attr = array();	
	
	$accent_color = redfish_get_option( 'accent-color' );
	
	$btn_wrapper_classes = array( 'btn-container' );
	$btn_classes = array();
	
		
	if( $featured_box_style == 'featured-box-style2' ) {
		$wrapper_classes[] = 'has-bg-color';
		$wrapper_style['background-color'] = $box_bg_color;
	}			
	elseif( $featured_box_style == 'featured-box-style3' ) {
		$wrapper_classes[] = 'has-bg-color';
		$wrapper_classes[] = 'icon-bottom';
		$wrapper_style['background-color'] = $box_bg_color;
	}	
	elseif( $featured_box_style == 'featured-box-style4' ) {
		$wrapper_classes[] = 'has-bg-image';	
		$box_bg_image_url = redfish_get_attachment_image( $box_bg_image, 'full' );
		
		if( $box_bg_image_url ) {
			$wrapper_style['background-image'] = 'url(' . $box_bg_image_url . ')';
		}
	}
	
	$icon_style[ 'font-size' ] = isset( $icon_size ) ? esc_attr( $icon_size ) : '30px'; 
	
	if( $icon_line_height ) {
		$icon_style[ 'line-height' ] = $icon_line_height;
	}

	if( $icon_type == 'selector' && $use_custom_color == 'on' && $icon_color ) {
		$icon_style[ 'color' ] = $icon_color;
	}
	elseif( $icon_type == 'selector' ) {
		$icon_style[ 'color' ] = $accent_color;
	}
	
	
	//upper heading style
	if( $upper_heading_font_size ) {
		$upper_heading_style[ 'font-size' ] = $upper_heading_font_size; 
	}
	
	if( $upper_heading_font_weight ) {
		$upper_heading_classes[] = 'wt-'.$upper_heading_font_weight; 
	}
	
	if( $upper_heading_color ) {
		$upper_heading_style[ 'color' ] = $upper_heading_color; 
	}

	//heading style
	if( $heading_font_size ) {
		$heading_style[ 'font-size' ] = $heading_font_size; 
	}
	
	if( $heading_font_weight ) {
		$heading_classes[] = 'wt-'.$heading_font_weight; 
	}
	
	if( $heading_color ) {
		$heading_style[ 'color' ] = $heading_color; 
	}	
	
	if( $desc_color ) {
		$desc_style[ 'color' ] = $desc_color; 
	}
		
	$btn_classes[] = $btn_style;
	
	$btn_classes[] = $btn_shape;
	
		
	if( $btn_shape == 'rounded' ) {
		$btn_css_style[ 'border-radius' ] = $btn_radius;
	}
	
	if( $btn_style == 'bordered' ) {
		
		$btn_css_style[ 'border-color' ] = $button_border_color;

		$btn_data_attr[ 'data-element' ] = 'button';
		
		$btn_data_attr[ 'data-outline-color' ] = $button_border_color;
		$btn_data_attr[ 'data-outline-hover-color' ] = $button_border_hover_color;
		$btn_data_attr[ 'data-outline-property' ] = 'border-color';
		
		$btn_css_style[ 'background-color' ] = $button_color;
				
		$btn_data_attr[ 'data-color' ] = $button_color;
		$btn_data_attr[ 'data-hover-color' ] = $button_hover_color;
		$btn_data_attr[ 'data-property' ] = 'background-color';
		
	}
	else {
		
		$btn_css_style[ 'background-color' ] = $button_color;		
		
		$btn_data_attr[ 'data-color' ] = $button_color;
		$btn_data_attr[ 'data-hover-color' ] = $button_hover_color;
		$btn_data_attr[ 'data-property' ] = 'background-color';
	}
	
	$btn_css_style[ 'color' ] = $button_text_color;
	
	$btn_css_style[ 'font-size' ] = $button_font_size;
	
	$btn_css_style[ 'font-weight' ] = $button_font_weight;
		
	$btn_classes[] = 'btn-small';
	
	$btn_classes[] = $button_text_hover_color;
	
	$btn_classes[] = 'custom-scroll';
	
	$type = redfish_get_option( 'icon-type' );
	
	// Enqueue needed icon font.
	vc_icon_element_fonts_enqueue( $type );
	
	$iconClass = isset( ${'icon_' . $type} ) ? esc_attr( ${'icon_' . $type} ) : 'fa fa-adjust';
		
	$css_featured_box = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css_featured_box, ' ' ), "redfish_featured_box", $atts );
	
	$wrapper_classes[] = esc_attr( $css_featured_box );
	
	
	$wrapper_classes = implode( ' ', $wrapper_classes );	
	
	$upper_heading_classes = implode( ' ', $upper_heading_classes );
	$heading_classes = implode( ' ', $heading_classes );
	$desc_classes = implode( ' ', $desc_classes );
	
	$btn_wrapper_classes  = implode( ' ', $btn_wrapper_classes );	
	$btn_classes  = implode( ' ', $btn_classes );		

?>

<div class="<?php echo esc_attr( $wrapper_classes ); ?>" <?php echo redfish_build_inline_style( $wrapper_style ); ?>>
	
	<div class="frd-box-heading-wrap vc_clearfix">
		
		<?php if ( $icon_type != 'none' && $featured_box_style != 'featured-box-style3' ) { ?>
		
			<div class="frd-box-graphic">

				<?php if ( $icon_type == 'selector' ) { ?>
					<div class="frd-box-icon" <?php echo redfish_build_inline_style( $icon_style ); ?> >
						<i class="<?php echo esc_attr( $iconClass ); ?>"></i>
					</div>
				<?php 		
				} 
				else { 
					
					$icon_img_url = redfish_get_attachment_image( $icon_img, 'full' ); 
								
					if( $icon_img_url ) { ?>	
								
						<div class="frd-box-img" <?php echo redfish_build_inline_style( $icon_style ); ?> >
							<img class="img-icon" src="<?php echo esc_url( $icon_img_url ); ?>" alt="<?php echo esc_attr( $heading ); ?>">		
						</div>
				
				<?php } 				
				}
				?>

			</div>
		
		<?php } ?>
		
		<div class="frd-box-heading">
			
			<?php if ( $upper_heading ) { ?>		
			<small class="<?php echo esc_attr( $upper_heading_classes ); ?>" <?php echo redfish_build_inline_style( $upper_heading_style ) ?> >
				<?php echo esc_html( $upper_heading ); ?>
			</small>
			<?php } ?>		
			
			<?php if ( $heading ) { ?>		
				<h3 class="<?php echo esc_attr( $heading_classes ); ?>" <?php echo redfish_build_inline_style( $heading_style ) ?> >
					<?php echo esc_html( $heading ); ?>
				</h3>					
			<?php } ?>		
			
		</div>
		
	</div>
	
	<p class="<?php echo esc_attr( $desc_classes ); ?>" <?php echo redfish_build_inline_style( $desc_style ) ?> >
		<?php echo esc_html( $desc ); ?>
	</p>
	
	<?php if( $btn_text ) { ?>
	
		<div class="<?php echo esc_attr( $btn_wrapper_classes ); ?>">	
			<a class="<?php echo esc_attr( $btn_classes ); ?>" <?php echo redfish_build_link_attr( $btn_link ); ?> <?php echo redfish_build_inline_style( $btn_css_style ); ?> <?php echo redfish_build_data_attr( $btn_data_attr ); ?> >
				<span><?php echo esc_html( $btn_text ); ?></span>
			</a>	
		</div>
	
	<?php } ?>
	
	<?php if ( $icon_type != 'none' && $featured_box_style == 'featured-box-style3' ) { ?>
		
		<div class="frd-box-graphic">

			<?php if ( $icon_type == 'selector' ) { ?>
				<div class="frd-box-icon" <?php echo redfish_build_inline_style( $icon_style ); ?> >
					<i class="<?php echo esc_attr( $iconClass ); ?>"></i>
				</div>
			<?php 		
			} 
			else { 
				
				$icon_img_url = redfish_get_attachment_image( $icon_img, 'full' ); 
								
				if( $icon_img_url ) { ?>					
								
					<div class="frd-box-img" <?php echo redfish_build_inline_style( $icon_style ); ?> >
						<img class="img-icon" src="<?php echo esc_url( $icon_img_url ); ?>" alt="<?php echo esc_attr( $heading ); ?>">		
					</div>
			
			<?php } 			
			}
			?>

		</div>
		
	<?php } ?>
	
</div>
