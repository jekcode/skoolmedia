<?php
if ( is_admin() ) {
return;
}
extract( vc_map_get_attributes( 'redfish_award', $atts ) );  
$award_data = (array) vc_param_group_parse_atts( $award_data );
?>
    <!-- Awards View -->
    <section class="grey-bg section-padding">
        <div class="container">
            <div class="row">
        <?php 
      
      foreach ( $award_data as $award ) {
      $award_description = !empty( $award['award_description'] ) ? $award['award_description'] : '1,200';
      $award_title = !empty( $award['award_title'] ) ? $award['award_title'] : 'schools';
      $award_icon = !empty( $award['award_icon'] ) ? $award['award_icon'] : 'fa fa-users';
      
      ?>
      <!-- Award -->
                <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-3">
                    <div class="shadow-sm bg-white award">
                       <!-- Award Title -->
                        <div class="award-title">
                             <?php echo esc_html( $award_title ); ?>
                        </div>
                        <!-- / Award Title -->
                        
                        <!-- Award Icon -->
                        <div class="award-icon">
                            <i class="<?php echo esc_html( $award_icon ); ?>"></i>
                        </div>
                        <!-- / Award Icon -->
                        
                        <!-- Award Description -->
                        <div class="award-desc">
                            <?php echo esc_html( $award_description ); ?>
                        </div>
                        <!-- / Award Description -->
                    </div>
                </div>
                <!-- / Award -->
       
      
        <?php } ?>
  
   <!-- / Award -->
            </div>
        </div>
    </section>
    <!-- / Awards View -->
