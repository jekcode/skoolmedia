<?php // archive-tr_team.php
get_header(); ?>

<!-- Gallery -->
    <section class="grey-bg section-padding-stats">
        <div class="container">

            <!-- Gallery Header -->
            <div class="row">

                <div class="col-3 mr-auto">
                    <h4>
                        Gallery
                    </h4>
                </div>
                    <!-- / Filter Gallery -->
                </div>

            </div>
            <!-- / Gallery Header -->

            <!-- Gallery Content -->
            <div class="container-fluid shadow-sm bg-white">
                <div class="gallery-content-holder">
                    <div class="row image-gallery">

                        <!-- Gallery Box with Light Box View -->

                         <?php while (have_posts()) : the_post() ?>

                        <!-- Gallery Image -->
                        <div class="col-6 col-md-3">
                            <a href="assets/img/BK0A1372.JPG" data-toggle="lightbox" data-gallery="main-gallery">
                                <div>
                                    <img src="assets/img/BK0A1372.JPG" class="img-fluid">
                                </div>

                                <!-- Image Details -->
                                <div class="bg-white p-2">
                                    <div class="gallery-image-title">
                                        <?php the_title(); ?>
                                    </div>

                                    <div class="gallery-image-desc">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                                <!-- / Image Details -->
                            </a>
                        </div>
                        <!-- / Gallery Image -->
                        <?php endwhile; ?>

                        <!-- / Gallery Box with Light Box View -->

                    </div>
                </div>
            </div>
            <!-- / Gallery Content -->

        </div>
    </section>
    <!-- / Gallery -->

<?php get_footer(); ?>