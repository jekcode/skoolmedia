<?php get_header(); ?>

<!-- Gallery -->
    <section class="grey-bg section-padding-stats">
        <div class="container">

            <div class="row news-view">
                <div class="col-md-2">
                </div>
                <!-- Gallery Content -->

                <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <div class="col-md-8">
                    <div class="view-news-title" style="background-image: url('assets/img/patrick-tomasso-71909-unsplash.jpg');">
                        <div class="blue-tint">
                            <h4>
                                <?php the_title(); ?>
                            </h4>
                            <p></p>
                        </div>

                    </div>
                    <div class="shadow-sm bg-white">
                        <div class="news-copy">
                        	<small>Posted on <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span> <?php _e( 'in ', 'skoolmedia' ); the_category(', '); // Separated by commas ?> | <?php the_tags( __( 'Tags: ', 'skoolmedia' ), ', ', '<br>'); // Separated by commas with a line break at the end ?></small> 
                            <p>
                                <?php the_content(); // Dynamic Content ?>
                            </p>
                            <hr>
                            <p><?php _e( 'Posted by ', 'skoolmedia' ); the_author(); ?></p>
                        </div>
                    

                    </div>

                </div>
                <!-- / Gallery Content -->
                <?php endwhile; ?>

				<?php else: ?>

					<!-- article -->
					<article>

						<h1><?php _e( 'Sorry, nothing to display.', 'skoolmedia' ); ?></h1>

					</article>
					<!-- /article -->

				<?php endif; ?>

                <!-- Gallery Content -->
                <!-- To be comtinued
                <div class="col-md-2">
                    <div class="shadow-sm bg-white">
                        Boss
                    </div>

                </div>
                -->
                <!-- / Gallery Content -->
            </div>

        </div>
    </section>
    <!-- / Gallery -->


<?php //get_sidebar(); ?>

<?php get_footer(); ?>
