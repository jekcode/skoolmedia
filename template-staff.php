<?php /* Template Name: Staff Page Template */ get_header(); ?>
<?php 

$getimage = tr_posts_field('banner_image');
$image = wp_get_attachment_image_url($getimage);

$background_image  = !empty( $image ) ?  $image : 'http://sm.test/wp-content/uploads/2018/11/patrick-tomasso-71909-unsplash.jpg';
$heading = !empty( tr_posts_field('heading') ) ? tr_posts_field('heading') : 'This is Skool Media';
$subheading = !empty( tr_posts_field('subheading') ) ? tr_posts_field('subheading') : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at auctor turpis, ac feugiat sapien. Maecenas auctor urna egestas, placerat felis a, ultrices dui. Nam quis convallis ex, eu pellentesque diam. Praesent non lacinia risus. Donec pharetra, ipsum non eleifend posuere, lectus nisi suscipit urna, in convallis lectus erat vitae metus. Integer pharetra et enim sed dapibus. Mauris semper purus ipsum, a accumsan tortor molestie sit amet.';
$color_tint = !empty( tr_posts_field('color_tint') ) ? tr_posts_field('color_tint') : 'green';

?>

  <!-- Hero Text Intro -->
    <section class="hero-basic" style="background-image: url('<?php echo $background_image ; ?>');">
        <div class="w-100 <?php echo $color_tint; ?>-tint">


            <div class="col-lg-9 mx-auto">
                <div class="hero-info">

                    <!-- About Us Page Title -->
                    <div class="title">
                        <h1>
                            <?php echo $heading; ?>
                        </h1>
                    </div>
                    <!-- / About Us Page Title -->

                    <!-- About Us Page Excerpt -->
                    <div class="copy">
                        <p>
                            <?php echo $subheading; ?>
                        </p>
                    </div>
                    <!-- / About Us Page Excerpt -->

                </div>
            </div>

            <!-- / Carousel -->
        </div>
    </section>
    <!-- / Hero Text Intro -->
<!-- Management -->
    <section class="section-padding">
        <div class="container">

            <div class="section-title">
                Our Management Team
            </div>

            <div class="section-sub-title">
            </div>

            <div class="col-lg-9 mx-auto">
                <!-- Management Row -->
                <div class="row">

                    <?php 

                $args = array(
                    'post_type'      => 'tr_team',
                    //'cat'            => '22,47,67',
                    'orderby'        => 'date',
                    'order'          => 'DESC',
                    'hide_empty'     => 1,
                    //'depth'          => 1,
                    //'posts_per_page' => 4,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'level',
                            'field' => 'slug',
                            'terms' => 'management-staff',
                        )
                    ),
                );

                // the query
                $the_query = new WP_Query( $args ); ?>

                <?php if ( $the_query->have_posts() ) : ?>

                    <?php //var_dump($the_query); ?>

                    <!-- pagination here -->



                    <!-- the loop -->
                    <?php

                     $count = 0;

                     while ( $the_query->have_posts() ) : $the_query->the_post();  $count++ ?>

                    <!-- Management Personnel -->
                    <div class="col-6 col-md-3">
                        <!-- Management Personnel Box -->
                        <div class="mng-box">
                            <div class="mng-content">
                                <div class="mng-details">
                                    <!-- Name of the Person -->
                                    <div class="mng-title">
                                        <?php the_title(); ?>
                                    </div>
                                    <!-- / Name of the Person -->

                                    <!-- Position of the Person -->
                                    <div class="mng-tag">
                                        <?php echo tr_posts_field("job_title"); ?>
                                    </div>
                                    <!-- / Position of the Person -->

                                    <!-- View more information on the Person -->
                                    <div class="mng-view">
                                        <a class="tip" href="#" data-toggle="modal" data-placement="top" title="View Profile" data-target="#<?php echo $count; ?>"><i class="fa fa-plus-square-o"></i></a>
                                    </div>
                                    <!-- View more information on the Person -->
                                </div>
                                <!-- Image of the Person. Must be square i.e height = width -->
                                <div class="mng-image">
                                    <img class="img-fluid" src="<?php
                                        echo wp_get_attachment_image_url(tr_posts_field("photo")); ?>">
                                </div>
                                <!-- / Image of the Person -->
                            </div>
                        </div>
                        <!-- / Management Personnel Box -->
                    </div>
                    <!-- / Management Personnel -->

                    <div class="modal fade mng-profile" id="<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="Management Profile" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">
                        <img class="img-fluid rounded-circle modal-img" src="<?php echo wp_get_attachment_image(tr_posts_field('photo'));?>" >
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tag">
                        <!-- Name of the Person -->
                        <div class="mng-title">
                            <?php the_title(); ?>
                        </div>
                        <!-- / Name of the Person -->

                        <!-- Position of the Person -->
                        <div class="mng-tag">
                             <?php echo tr_posts_field("job_title"); ?>
                        </div>
                        <!-- / Position of the Person -->
                    </div>

                    <!-- History of the Person -->
                    <div>
                        <p>
                           <?php the_content(); ?>
                        </p>
                    </div>
                    <!-- / History of the Person -->
                </div>
                <div class="modal-footer">
                    <div class="ft-links">

                        <!-- Person's Social link -->
                        <li>
                            <a class="social-link" target="_blank" href="<?php echo tr_posts_field('facebook'); ?>">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_posts_field('instagram'); ?>">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_posts_field('twitter'); ?>">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_posts_field('youtube'); ?>">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                        <!-- / Person's Social link -->

                    </div>
                </div>
            </div>
        </div>
    </div>



<?php endwhile; ?>
                    <!-- end of the loop -->

                    <?php wp_reset_postdata(); ?>

                <?php else : ?>
                    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>

                </div>
                <!-- / Management Row -->
            </div>

        </div>
    </section>
    <!-- / Management -->

    <!-- Management -->
    <section class="section-padding grey-bg">
        <div class="container">

            <div class="section-title">
                Staff Members
            </div>

            <div class="section-sub-title">
                <a class="view-staffMembers" data-toggle="collapse" href="#staffMembers" role="button" aria-expanded="false"><i class="fa fa-chevron-down"></i></a>
            </div>

            <div id="staffMembers" class="col-lg-9 mx-auto collapse">
                <!-- Management Row -->
                <div class="row">

                      <?php 

                $args = array(
                    'post_type'      => 'tr_team',
                    //'cat'            => '22,47,67',
                    'orderby'        => 'date',
                    'order'          => 'DESC',
                    'hide_empty'     => 1,
                    //'depth'          => 1,
                    //'posts_per_page' => 4,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'level',
                            'field' => 'slug',
                            'terms' => 'staff-members',
                        )
                    ),
                );

                // the query
                $the_query = new WP_Query( $args ); ?>

                <?php if ( $the_query->have_posts() ) : ?>

                    <?php //var_dump($the_query); ?>

                    <!-- pagination here -->



                    <!-- the loop -->
                    <?php

                     $counter = 0;

                     while ( $the_query->have_posts() ) : $the_query->the_post();  $counter++ ?>



                    <!-- Management Personnel -->
                    <div class="col-6 col-md-3">
                        <!-- Management Personnel Box -->
                        <div class="mng-box">
                            <div class="mng-content">
                                <div class="mng-details">
                                    <!-- Name of the Person -->
                                    <div class="mng-title">
                                        <?php the_title(); ?>
                                    </div>
                                    <!-- / Name of the Person -->

                                    <!-- Position of the Person -->
                                    <div class="mng-tag">
                                        <?php echo tr_posts_field("job_title"); ?>
                                    </div>
                                    <!-- / Position of the Person -->

                                    <!-- View more information on the Person -->
                                    <div class="mng-view">
                                        <a class="tip" href="#" data-toggle="modal" data-placement="top" title="View Profile" data-target="#<?php echo $counter; ?>"><i class="fa fa-plus-square-o"></i></a>
                                    </div>
                                    <!-- View more information on the Person -->
                                </div>
                                <!-- Image of the Person. Must be square i.e height = width -->
                                <div class="mng-image">
                                    <img class="img-fluid" src="<?php
            echo wp_get_attachment_image_url(tr_posts_field("photo")); ?>">
                                </div>
                                <!-- / Image of the Person -->
                            </div>
                        </div>
                        <!-- / Management Personnel Box -->
                    </div>
                    <!-- / Management Personnel -->

                    <div class="modal fade mng-profile" id="<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="Management Profile" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">
                        <img class="img-fluid rounded-circle modal-img" src="<?php echo wp_get_attachment_image(tr_posts_field('photo'));?>" 
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tag">
                        <!-- Name of the Person -->
                        <div class="mng-title">
                            <?php the_title(); ?>
                        </div>
                        <!-- / Name of the Person -->

                        <!-- Position of the Person -->
                        <div class="mng-tag">
                             <?php echo tr_posts_field("job_title"); ?>
                        </div>
                        <!-- / Position of the Person -->
                    </div>

                    <!-- History of the Person -->
                    <div>
                        <p>
                           <?php the_content(); ?>
                        </p>
                    </div>
                    <!-- / History of the Person -->
                </div>
                <div class="modal-footer">
                    <div class="ft-links">

                        <!-- Person's Social link -->
                        <li>
                            <a class="social-link" target="_blank" href="<?php echo tr_posts_field('facebook'); ?>">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_posts_field('instagram'); ?>">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_posts_field('twitter'); ?>">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a class="social-link" target="_blank" href="<?php echo tr_posts_field('youtube'); ?>">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                        <!-- / Person's Social link -->

                    </div>
                </div>
            </div>
        </div>
    </div>



<?php endwhile; ?>
                    <!-- end of the loop -->

                    <?php wp_reset_postdata(); ?>

                <?php else : ?>
                    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>

   











                        
                </div>
                <!-- / Management Row -->
            </div>

        </div>
    </section>
    <!-- / Management -->




<?php get_footer(); ?>


    
