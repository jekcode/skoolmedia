<?php

/*
Element Description: VC Info Box
*/
 
// Element Class 
class vcHomeTopBanner extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_home_top_banner_mapping' ) );
        add_shortcode( 'vc_home_top_banner', array( $this, 'vc_home_top_banner_html' ) );
    }
     
    // Element Mapping
    public function vc_home_top_banner_mapping() {
         
       
         
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
    }
         
    // Map the block with vc_map()
    vc_map( 
  
        array(
            'name' => __('VC Home Top Banner', 'skoolmedia'),
            'base' => 'vc_home_top_banner',
            'description' => __('A simple VC box', 'skoolmedia'), 
            'category' => __('Ladders Elements', 'skoolmedia'),   
            'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',            
            'params' => array(   
                      
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'URL', 'skoolmedia' ),
                    'param_name' => 'url',
                    'value' => __( 'Image URL', 'skoolmedia' ),
                    'description' => __( 'Image URL', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Custom Group',
                )               
                     
            )
        )
    );                                
        
}                         
    // Element HTML
    public function vc_home_top_banner_html( $atts ) {
         
        //public function vc_informationbox_html( $atts ) {
     
            // Params extraction
            extract(
                shortcode_atts(
                    array(
                        'url'   => '',
                        //'text' => '',
                    ), 
                    $atts
                )
            );
             
            // Fill $html var with data
            $html = '

            <section class="hero-img" style="background-image: url(' . $url . ');">
                <div class="w-100">
                    <div class="display-tr">

                    </div>
                </div>
            </section>
            ';      
             
            return $html;
             
        }
         
     
     
} // End Element Class
 
// Element Class Init
new vcHomeTopBanner();  
   