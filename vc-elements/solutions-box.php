<?php
/**
 * Registers the solutions shortcode and adds it to the Visual Composer 
 */

class WPBakeryShortCode_redfish_solutions extends WPBakeryShortCode {
	
	protected function content( $atts, $content = null ) {
		
		ob_start();
		
		if( locate_template( 'custom-templates/solutions.php' ) != '' ) {
			include( locate_template( 'custom-templates/solutions.php' ) );
		}
		
		return ob_get_clean();
	}	
}

if ( ! function_exists( 'redfish_solutions_vc_map' ) ) {
	
	function redfish_solutions_vc_map() {

		return array(
			"name"					=> esc_html__( "solutionss Box", 'redfish' ),
			"description"			=> esc_html__( "Add a time line", 'redfish' ),
			"base"					=> "redfish_solutions",
			//'category' => __('RDM Elements', 'text-domain'),  
			"category"				=> __('Ladders Elements', 'text-domain'), 
			"icon"					=> "redfish-solutions-icon",			
			"params"				=> array(					
				array(
					"type"			=> "param_group",
					"heading"		=> esc_html__( 'solutions content', 'redfish' ),
					"param_name"	=> "solutions_data",					
					"description"	=> esc_html__( 'Enter solutions data.', 'redfish' ),			
					"value"			=> urlencode( json_encode( array(
										array('solutions_name' => 'Library Room',
												'solutions_description' => 'We create informal atmosphere where students can relax and at the same time read and socialize.',
												'solutions_icon' => 'fa fa-users',
												'solutions_image_url' => 'http://example.comhttp://sm.test/wp-content/uploads/2018/11/neonbrand-426918-unsplash.jpg',										
										),
										array('solutions_name' => 'Library Room',
												'solutions_description' => 'We create informal atmosphere where students can relax and at the same time read and socialize.',
												'solutions_icon' => 'fa fa-users',
												'solutions_image_url' => 'http://sm.test/wp-content/uploads/2018/11/neonbrand-426918-unsplash.jpg',							
										),
										
										
										) ) ),					
					"params"		=> array(
											array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'solutions Name', 'fbnquest' ),
				'param_name' => 'solutions_name',
				'value' => 'Wealth Management',
			),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Short Description', 'fbnquest' ),
				'param_name' => 'solutions_description',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Read More Link', 'fbnquest' ),
				'param_name' => 'solutions_icon',
				'value' => 'fa fa-users',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Read More Text', 'fbnquest' ),
				'param_name' => 'solutions_image_url',
				'value' => 'http://example.com',
			),									
										),
				),
				
			)
		);
	}

}

vc_lean_map( 'redfish_solutions', 'redfish_solutions_vc_map' );
