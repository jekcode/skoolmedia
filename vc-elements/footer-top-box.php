<?php

/*
Element Description: VC Info Box
*/
 
// Element Class 
class vcfooter_topBox extends WPBakeryShortCode {

    //$element = '';
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_footer_top_box_mapping' ) );
        add_shortcode( 'vc_footer_top_box', array( $this, 'vc_footer_top_box_html' ) );
    }
     
    // Element Mapping
    public function vc_footer_top_box_mapping() {
         
       
         
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
    }
         
    // Map the block with vc_map()
    vc_map( 
  
        array(
            'name' => __('footer_top Box 1', 'skoolmedia'),
            'base' => 'vc_footer_top_box',
            'description' => __('footer_top box with center alignment', 'skoolmedia'), 
            'category' => __('Ladders Elements', 'skoolmedia'),   
            'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',            
            'params' => array(   
                      
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Title', 'skoolmedia' ),
                    'param_name' => 'section_title',
                    'value' => __( 'Welcome to Skool Media', 'skoolmedia' ),
                    'description' => __( 'Title', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'footer_top Box Content',
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Sub Title', 'skoolmedia' ),
                    'param_name' => 'section_sub_title',
                    'value' => __( 'Your 21st Century School', 'skoolmedia' ),
                    'description' => __( 'Sub Title', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'footer_top Box Content',
                ),
                array(
                    'type' => 'textarea',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Content', 'skoolmedia' ),
                    'param_name' => 'section_content',
                    'value' => __( 'Content', 'skoolmedia' ),
                    'description' => __( 'Content', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'footer_top Box Content',
                ), 
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Content', 'skoolmedia' ),
                    'param_name' => 'section_class',
                    'value' => __( 'class', 'skoolmedia' ),
                    'description' => __( 'Content', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'footer_top Box Content',
                ),                   
                     
            )
        )
    );                                
        
}                         
    // Element HTML
    public function vc_footer_top_box_html( $atts ) {
         
        //public function vc_informationbox_html( $atts ) {
     
            // Params extraction
            extract(
                shortcode_atts(
                    array(
                        'section_title'   => '',
                        'section_sub_title' => '',
                        'section_content' => '',
                        'section_class' => '',
                    ), 
                    $atts
                )
            );
             
            // Fill $html var with data
            $html = '
            <section class="section-padding ' . $section_class . '">
        <div class="container">
            <!-- Section title -->
            <div class="section-title">
                ' . $section_title . '
            </div>
            <!-- / Section title -->

            <!-- Section sub title -->
            <div class="section-sub-title">
            </div>
            <!-- / Section sub title -->

            <div class="col-md-9 mx-auto">
                <p>
                   ' . $section_content . '
                </p>
            </div>
        </div>
    </section>

            
            ';      
             
            return $html;
             
        }
         
     
     
} // End Element Class
 
// Element Class Init
new vcfooter_topBox();  
   