<?php

/*
Element Description: VC Info Box
*/
 
// Element Class 
class vcFloatingBox2 extends WPBakeryShortCode {

    //$element = '';
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_floating_box2_mapping' ) );
        add_shortcode( 'vc_floating_box2', array( $this, 'vc_floating_box2_html' ) );
    }
     
    // Element Mapping
    public function vc_floating_box2_mapping() {
         
       
         
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
    }
         
    // Map the block with vc_map()
    vc_map( 
  
        array(
            'name' => __('Floating Box 2', 'skoolmedia'),
            'base' => 'vc_floating_box2',
            'description' => __('Floating box with center alignment', 'skoolmedia'), 
            'category' => __('Ladders Elements', 'skoolmedia'),   
            'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',            
            'params' => array(   
                      
                array(
                    'type' => 'textfield',
                    'holder' => 'h3',
                    'class' => 'title-class',
                    'heading' => __( 'Section Title', 'skoolmedia' ),
                    'param_name' => 'float_title',
                    'value' => __( 'Welcome to Skool Media', 'skoolmedia' ),
                    'description' => __( 'Title', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Floating Box Content',
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section URL', 'skoolmedia' ),
                    'param_name' => 'float_url',
                    'value' => __( 'Your 21st Century School', 'skoolmedia' ),
                    'description' => __( 'Sub Title', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Floating Box Content',
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section URL Label', 'skoolmedia' ),
                    'param_name' => 'float_urltext',
                    'value' => __( 'Your 21st Century School', 'skoolmedia' ),
                    'description' => __( 'Sub Title', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Floating Box Content',
                ),
                array(
                    'type' => 'textarea',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Content', 'skoolmedia' ),
                    'param_name' => 'float_content',
                    'value' => __( 'Content', 'skoolmedia' ),
                    'description' => __( 'Content', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Floating Box Content',
                ),              
                     
            )
        )
    );                                
        
}                         
    // Element HTML
    public function vc_floating_box2_html( $atts ) {
         
        //public function vc_informationbox_html( $atts ) {
     
            // Params extraction
            extract(
                shortcode_atts(
                    array(
                        'float_title'   => '',
                        //'section_sub_title' => '',
                        'float_content' => '',
                        'float_url' => '',
                        'float_urltext' => '',
                    ), 
                    $atts
                )
            );
             
            // Fill $html var with data
            $html = '
            <section class="grey-bg section-padding">
                <div class="container"> 
                    <div class="col-lg-8 mx-auto">
                        <div class="floating-box">
                            <div class="section-title">
                                <p>
                                    ' . $float_title. '
                                </p>
                            </div>

                            <div class="section-sub-title">
                            </div>

                            <div class="copy">
                                 ' . $float_content. '
                            
                            </div>

                            <div class="btn-holder">
                                <a class="btn btn-red-primary" href=" ' . $float_url . '">
                                     ' . $float_urltext. '
                                </a>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </section>';      
             
            return $html;
             
        }
         
     
     
} // End Element Class
 
// Element Class Init
new vcFloatingBox2();  
   