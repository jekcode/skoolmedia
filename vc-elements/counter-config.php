<?php
/**
 * Registers the counter shortcode and adds it to the Visual Composer 
 */

class WPBakeryShortCode_redfish_counter extends WPBakeryShortCode {
	
	protected function content( $atts, $content = null ) {
		
		ob_start();
		
		if( locate_template( 'custom-templates/counters.php' ) != '' ) {
			include( locate_template( 'custom-templates/counters.php' ) );
		}
		
		return ob_get_clean();
	}	
}

if ( ! function_exists( 'redfish_counter_vc_map' ) ) {
	
	function redfish_counter_vc_map() {

		return array(
			"name"					=> esc_html__( "Counters Box", 'redfish' ),
			"description"			=> esc_html__( "Add a time line", 'redfish' ),
			"base"					=> "redfish_counter",
			//'category' => __('RDM Elements', 'text-domain'),  
			"category"				=> __('Ladders Elements', 'text-domain'), 
			"icon"					=> "redfish-counter-icon",			
			"params"				=> array(					
				array(
					"type"			=> "param_group",
					"heading"		=> esc_html__( 'counter content', 'redfish' ),
					"param_name"	=> "counter_data",					
					"description"	=> esc_html__( 'Enter counter data.', 'redfish' ),			
					"value"			=> urlencode( json_encode( array(
										array('counter_number' => ' 1,234',
												'counter_title' => 'schools',
												'counter_icon' => 'fa fa-institution',
																					
										),
										array('counter_number' => ' 1,234',
												'counter_title' => 'students',
												'counter_icon' => 'fa fa-users',							
										),
										array('counter_number' => ' 68',
												'counter_title' => 'Projects',
												'counter_icon' => 'fa fa-flag-o',							
										),
										
										
										) ) ),					
					"params"		=> array(
											array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Counter Number', 'fbnquest' ),
				'param_name' => 'counter_number',
				'value' => '2,000',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Title', 'fbnquest' ),
				'param_name' => 'counter_title',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Read More Link', 'fbnquest' ),
				'param_name' => 'counter_icon',
				'value' => 'fa fa-users',
			),									
										),
				),
				
			)
		);
	}

}

vc_lean_map( 'redfish_counter', 'redfish_counter_vc_map' );
