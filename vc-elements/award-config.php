<?php
/**
 * Registers the award shortcode and adds it to the Visual Composer 
 */

class WPBakeryShortCode_redfish_award extends WPBakeryShortCode {
	
	protected function content( $atts, $content = null ) {
		
		ob_start();
		
		if( locate_template( 'custom-templates/award.php' ) != '' ) {
			include( locate_template( 'custom-templates/award.php' ) );
		}
		
		return ob_get_clean();
	}	
}

if ( ! function_exists( 'redfish_award_vc_map' ) ) {
	
	function redfish_award_vc_map() {

		return array(
			"name"					=> esc_html__( "Awards Box", 'redfish' ),
			"description"			=> esc_html__( "Add a time line", 'redfish' ),
			"base"					=> "redfish_award",
			//'category' => __('RDM Elements', 'text-domain'),  
			"category"				=> __('Ladders Elements', 'text-domain'), 
			"icon"					=> "redfish-award-icon",			
			"params"				=> array(					
				array(
					"type"			=> "param_group",
					"heading"		=> esc_html__( 'award content', 'redfish' ),
					"param_name"	=> "award_data",					
					"description"	=> esc_html__( 'Enter award data.', 'redfish' ),			
					"value"			=> urlencode( json_encode( array(
										array('award_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
												'award_title' => 'Award and Recognition titl',
												'award_icon' => 'fa fa-institution',
																					
										),
										array('award_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
												'award_title' => 'students',
												'award_icon' => 'fa fa-users',							
										),
										array('award_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
												'award_title' => 'Projects',
												'award_icon' => 'fa fa-flag-o',							
										),
										
										
										) ) ),					
					"params"		=> array(
											array(
				'type' => 'textarea',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'award description', 'fbnquest' ),
				'param_name' => 'award_description',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Title', 'fbnquest' ),
				'param_name' => 'award_title',
				'value' => 'Award and Recognition title',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Read More Link', 'fbnquest' ),
				'param_name' => 'award_icon',
				'value' => 'fa fa-users',
			),									
										),
				),
				
			)
		);
	}

}

vc_lean_map( 'redfish_award', 'redfish_award_vc_map' );
