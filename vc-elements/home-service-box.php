<?php
/**
 * Registers the service shortcode and adds it to the Visual Composer 
 */

class WPBakeryShortCode_redfish_service extends WPBakeryShortCode {
	
	protected function content( $atts, $content = null ) {
		
		ob_start();
		
		if( locate_template( 'custom-templates/services.php' ) != '' ) {
			include( locate_template( 'custom-templates/services.php' ) );
		}
		
		return ob_get_clean();
	}	
}

if ( ! function_exists( 'redfish_service_vc_map' ) ) {
	
	function redfish_service_vc_map() {

		return array(
			"name"					=> esc_html__( "Home Services Box", 'redfish' ),
			"description"			=> esc_html__( "Add a time line", 'redfish' ),
			"base"					=> "redfish_service",
			//'category' => __('RDM Elements', 'text-domain'),  
			"category"				=> __('Ladders Elements', 'text-domain'), 
			"icon"					=> "redfish-service-icon",			
			"params"				=> array(					
				array(
					"type"			=> "param_group",
					"heading"		=> esc_html__( 'service content', 'redfish' ),
					"param_name"	=> "service_data",					
					"description"	=> esc_html__( 'Enter service data.', 'redfish' ),			
					"value"			=> urlencode( json_encode( array(
										array('service_name' => 'Library Room',
												'service_description' => 'We create informal atmosphere where students can relax and at the same time read and socialize.',
												'service_icon' => 'fa fa-users',
												'service_image_url' => 'http://sm.test/wp-content/uploads/2018/11/neonbrand-426918-unsplash.jpg',										
										),
										array('service_name' => 'Library Room',
												'service_description' => 'We create informal atmosphere where students can relax and at the same time read and socialize.',
												'service_icon' => 'fa fa-users',
												'service_image_url' => 'http://sm.test/wp-content/uploads/2018/11/neonbrand-426918-unsplash.jpg',							
										),
										
										
										) ) ),					
					"params"		=> array(
											array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Service Name', 'fbnquest' ),
				'param_name' => 'service_name',
				'value' => 'Wealth Management',
			),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Short Description', 'fbnquest' ),
				'param_name' => 'service_description',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Read More Link', 'fbnquest' ),
				'param_name' => 'service_icon',
				'value' => 'fa fa-users',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'class' => '',
				'admin_label' => true,
				'heading' => __( 'Read More Text', 'fbnquest' ),
				'param_name' => 'service_image_url',
				'value' => 'http://example.com',
			),									
										),
				),
				
			)
		);
	}

}

vc_lean_map( 'redfish_service', 'redfish_service_vc_map' );
