<?php

/*
Element Description: VC Info Box
*/
 
// Element Class 
class vciconBox extends WPBakeryShortCode {

    //$element = '';
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_icon_box_mapping' ) );
        add_shortcode( 'vc_icon_box', array( $this, 'vc_icon_box_html' ) );
    }
     
    // Element Mapping
    public function vc_icon_box_mapping() {
         
       
         
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
    }
         
    // Map the block with vc_map()
    vc_map( 
  
        array(
            'name' => __('Benefit Box 1', 'skoolmedia'),
            'base' => 'vc_icon_box',
            'description' => __('SKool Media Career Benefits box', 'skoolmedia'), 
            'category' => __('Ladders Elements', 'skoolmedia'),   
            'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',            
            'params' => array(   
                      
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Title', 'skoolmedia' ),
                    'param_name' => 'benefit_title',
                    'value' => __( 'Welcome to Skool Media', 'skoolmedia' ),
                    'description' => __( 'Title', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'icon Box Content',
                ),
                array(
                    'type' => 'textarea',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Content', 'skoolmedia' ),
                    'param_name' => 'benefit_content',
                    'value' => __( 'Content', 'skoolmedia' ),
                    'description' => __( 'Content', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'icon Box Content',
                ), 
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Content', 'skoolmedia' ),
                    'param_name' => 'benefit_icon',
                    'value' => __( 'fa fa-user', 'skoolmedia' ),
                    'description' => __( 'Content', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'icon Box Content',
                ),                   
                     
            )
        )
    );                                
        
}                         
    // Element HTML
    public function vc_icon_box_html( $atts ) {
         
        //public function vc_informationbox_html( $atts ) {
     
            // Params extraction
            extract(
                shortcode_atts(
                    array(
                        'benefit_title'   => '',
                        //'section_sub_title' => '',
                        'benefit_content' => '',
                        'benefit_icon' => '',
                    ), 
                    $atts
                )
            );
             
            // Fill $html var with data
            $html = '
            <!-- Single Benefit -->
            <div class="benefit">
                <div class="benefit-box shadow-sm">
                    <div class="benefit-icon">
                        <i class="' . $benefit_icon . '"></i>
                    </div>

                    <div class="benefit-title">
                        <p>
                           ' . $benefit_title . '
                        </p>
                    </div>

                    <div class="benefit-excerpt">
                        <p>
                           ' . $benefit_content . '
                        </p>
                    </div>
                </div>
            </div>
            <!-- /Single Benefit -->

            ';      
             
            return $html;
             
        }
         
     
     
} // End Element Class
 
// Element Class Init
new vciconBox();  
   