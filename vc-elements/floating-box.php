<?php

/*
Element Description: VC Info Box
*/
 
// Element Class 
class vcFloatingBox extends WPBakeryShortCode {

    //$element = '';
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_floating_box_mapping' ) );
        add_shortcode( 'vc_floating_box', array( $this, 'vc_floating_box_html' ) );
    }
     
    // Element Mapping
    public function vc_floating_box_mapping() {
         
       
         
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
    }
         
    // Map the block with vc_map()
    vc_map( 
  
        array(
            'name' => __('Floating Box 1', 'skoolmedia'),
            'base' => 'vc_floating_box',
            'description' => __('FLoating box with center alignment', 'skoolmedia'), 
            'category' => __('Ladders Elements', 'skoolmedia'),   
            'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',            
            'params' => array(   
                      
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Title', 'skoolmedia' ),
                    'param_name' => 'section_title',
                    'value' => __( 'Welcome to Skool Media', 'skoolmedia' ),
                    'description' => __( 'Title', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Floating Box Content',
                ),
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Sub Title', 'skoolmedia' ),
                    'param_name' => 'section_sub_title',
                    'value' => __( 'Your 21st Century School', 'skoolmedia' ),
                    'description' => __( 'Sub Title', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Floating Box Content',
                ),
                array(
                    'type' => 'textarea',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Content', 'skoolmedia' ),
                    'param_name' => 'section_content',
                    'value' => __( 'Content', 'skoolmedia' ),
                    'description' => __( 'Content', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Floating Box Content',
                ), 
                array(
                    'type' => 'textfield',
                    'holder' => 'div',
                    'class' => 'title-class',
                    'heading' => __( 'Section Content', 'skoolmedia' ),
                    'param_name' => 'section_class',
                    'value' => __( 'grey-bg', 'skoolmedia' ),
                    'description' => __( 'Content', 'skoolmedia' ),
                    'admin_label' => false,
                    'weight' => 0,
                    'group' => 'Floating Box Content',
                ),                   
                     
            )
        )
    );                                
        
}                         
    // Element HTML
    public function vc_floating_box_html( $atts ) {
         
        //public function vc_informationbox_html( $atts ) {
     
            // Params extraction
            extract(
                shortcode_atts(
                    array(
                        'section_title'   => '',
                        'section_sub_title' => '',
                        'section_content' => '',
                        'section_class' => '',
                    ), 
                    $atts
                )
            );
             
            // Fill $html var with data
            $html = '

            <section class="section-padding ' . $section_class . '" style="padding-bottom:0;">
            <div class="container">

            <div class="col-lg-8 mx-auto">
                <div class="floating-box">
                    <div class="section-title">
                        <p>
                            ' . $section_title . '
                        </p>
                    </div>

                    <div class="section-sub-title">
                        <p>
                           ' . $section_sub_title . '
                        </p>
                    </div>

                    <div>
                        ' . $section_content . '
                    </div>
                </div>
            </div>
</div>
</section>
            ';      
             
            return $html;
             
        }
         
     
     
} // End Element Class
 
// Element Class Init
new vcFloatingBox();  
   