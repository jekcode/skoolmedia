<?php /* Template Name: Schools Page Template */ get_header(); ?>
<?php 

$getimage = tr_posts_field('banner_image');
$image = wp_get_attachment_image_url($getimage);

$background_image  = !empty( $image ) ?  $image : 'http://sm.test/wp-content/uploads/2018/11/patrick-tomasso-71909-unsplash.jpg';
$heading = !empty( tr_posts_field('heading') ) ? tr_posts_field('heading') : 'This is Skool Media';
$subheading = !empty( tr_posts_field('subheading') ) ? tr_posts_field('subheading') : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at auctor turpis, ac feugiat sapien. Maecenas auctor urna egestas, placerat felis a, ultrices dui. Nam quis convallis ex, eu pellentesque diam. Praesent non lacinia risus. Donec pharetra, ipsum non eleifend posuere, lectus nisi suscipit urna, in convallis lectus erat vitae metus. Integer pharetra et enim sed dapibus. Mauris semper purus ipsum, a accumsan tortor molestie sit amet.';
$color_tint = !empty( tr_posts_field('color_tint') ) ? tr_posts_field('color_tint') : 'green';

?>

<!-- Hero Text Intro -->
    <section class="hero-basic" style="background-image: url('<?php echo $background_image ; ?>');">
        <div class="w-100 <?php echo $color_tint; ?>-tint">


            <div class="col-lg-9 mx-auto">
                <div class="hero-info">

                    <!-- About Us Page Title -->
                    <div class="title">
                        <h1>
                            <?php echo $heading; ?>
                        </h1>
                    </div>
                    <!-- / About Us Page Title -->

                    <!-- About Us Page Excerpt -->
                    <div class="copy">
                        <p>
                            <?php echo $subheading; ?>
                        </p>
                    </div>
                    <!-- / About Us Page Excerpt -->

                </div>
            </div>

            <!-- / Carousel -->
        </div>
    </section>
    <!-- / Hero Text Intro -->


<!-- Tested and trusted -->
<section class="grey-bg section-padding">
    <div class="container">

        <!-- Section title -->
        <!--<div class="section-title">
            Tested and Trusted
        </div>-->

        <div class="section-sub-title">
            In a bid to enhance the quality of education infrastructure and improve the teaching-learning process in our schools, Skool Media currently supports over 24 schools modern education technology infrastructure like a digital classroom, technology experience centres, library facilities, modern laboratory and other educational resources.
        </div>
        <!-- /Section title -->

        <div class="col-lg-7 mx-auto">
            <div class="testimonial-row">
                <div id="tt-logo" class="carousel slide" data-ride="carousel" data-pause="hover" data-interval="5000">
                    <div class="carousel-inner w-100 row mx-auto">


                        <?php 

                $args = array(
                    'post_type'      => 'tr_portfolio',
                    //'cat'            => '22,47,67',
                    'orderby'        => 'date',
                    'order'          => 'ASC',
                    'hide_empty'     => 1,
                    //'depth'          => 1,
                    'posts_per_page' => -1
                );

                // the query
                $the_query = new WP_Query( $args );?>

                        <?php if ( $the_query->have_posts() ) : ?>

                        <?php $count = 0; ?>
                        <!-- the loop -->
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $count++; ?>

                        <div class="carousel-item col-4 <?php if ($count == 1) {echo 'active';} ?>">
                            <!-- Testimonial Logo -->
                                <div class="testimonial-logo">
                                    <img class="img-fluid mx-auto d-block" src="<?php echo wp_get_attachment_image_url(tr_posts_field('school_logo')); ?>">
                                </div>
                            <!-- / Testimonial Logo -->
                        </div>

                        <?php endwhile; ?>
                        <!-- end of the loop -->

                        <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                        <p>
                            <?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?>
                        </p>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- /Tested and trusted -->

<?php get_footer(); ?>


    
